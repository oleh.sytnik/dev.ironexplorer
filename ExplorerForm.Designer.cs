using IonShard.Shell.Components;

namespace IonShard.Shell {
    partial class ShellExplorer {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ShellExplorer));
            this.treeView = new ShellTreeView();
            this.shellView = new ShellView();
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.placesToolbar1 = new PlacesToolbar();
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.fileMenu = new System.Windows.Forms.MenuItem();
            this.dummyMenuItem = new System.Windows.Forms.MenuItem();
            this.viewMenu = new System.Windows.Forms.MenuItem();
            this.refreshMenu = new System.Windows.Forms.MenuItem();
            this.toolBar = new System.Windows.Forms.ToolBar();
            this.backButton = new System.Windows.Forms.ToolBarButton();
            this.backButtonMenu = new System.Windows.Forms.ContextMenu();
            this.forwardButton = new System.Windows.Forms.ToolBarButton();
            this.forwardButtonMenu = new System.Windows.Forms.ContextMenu();
            this.upButton = new System.Windows.Forms.ToolBarButton();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.fileDialogToolbar1 = new FileDialogToolbar();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.splitContainer4 = new System.Windows.Forms.SplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).BeginInit();
            this.splitContainer4.Panel1.SuspendLayout();
            this.splitContainer4.Panel2.SuspendLayout();
            this.splitContainer4.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeView
            // 
            this.treeView.AllowDrop = true;
            this.treeView.BackColor = System.Drawing.Color.Azure;
            this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.treeView.Location = new System.Drawing.Point(0, 0);
            this.treeView.Name = "treeView";
            this.treeView.ShellView = this.shellView;
            this.treeView.Size = new System.Drawing.Size(267, 485);
            this.treeView.TabIndex = 0;
            this.treeView.Text = "shellTreeView1";
            // 
            // shellView
            // 
            this.shellView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.shellView.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.shellView.Location = new System.Drawing.Point(0, 0);
            this.shellView.Name = "shellView";
            this.shellView.Size = new System.Drawing.Size(537, 485);
            this.shellView.StatusBar = this.statusBar;
            this.shellView.TabIndex = 0;
            this.shellView.Navigated += new System.EventHandler(this.shellView_Navigated);
            // 
            // statusBar
            // 
            this.statusBar.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.statusBar.Location = new System.Drawing.Point(0, 518);
            this.statusBar.Name = "statusBar";
            this.statusBar.ShowPanels = true;
            this.statusBar.Size = new System.Drawing.Size(886, 25);
            this.statusBar.TabIndex = 1;
            // 
            // placesToolbar1
            // 
            this.placesToolbar1.AutoScrollMargin = new System.Drawing.Size(0, 0);
            this.placesToolbar1.AutoScrollMinSize = new System.Drawing.Size(0, 0);
            this.placesToolbar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.placesToolbar1.Location = new System.Drawing.Point(0, 0);
            this.placesToolbar1.Name = "placesToolbar1";
            this.placesToolbar1.ShellView = this.shellView;
            this.placesToolbar1.Size = new System.Drawing.Size(76, 485);
            this.placesToolbar1.TabIndex = 9;
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.fileMenu,
            this.viewMenu});
            // 
            // fileMenu
            // 
            this.fileMenu.Index = 0;
            this.fileMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.dummyMenuItem});
            this.fileMenu.Text = "&File";
            this.fileMenu.Popup += new System.EventHandler(this.fileMenu_Popup);
            // 
            // dummyMenuItem
            // 
            this.dummyMenuItem.Index = 0;
            this.dummyMenuItem.Text = "Dummy";
            this.dummyMenuItem.Visible = false;
            // 
            // viewMenu
            // 
            this.viewMenu.Index = 1;
            this.viewMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
            this.refreshMenu});
            this.viewMenu.Text = "&View";
            // 
            // refreshMenu
            // 
            this.refreshMenu.Index = 0;
            this.refreshMenu.Shortcut = System.Windows.Forms.Shortcut.F5;
            this.refreshMenu.Text = "&Refresh";
            this.refreshMenu.Click += new System.EventHandler(this.refreshMenu_Click);
            // 
            // toolBar
            // 
            this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
            this.backButton,
            this.forwardButton,
            this.upButton});
            this.toolBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolBar.DropDownArrows = true;
            this.toolBar.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolBar.ImageList = this.imageList;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.ShowToolTips = true;
            this.toolBar.Size = new System.Drawing.Size(103, 28);
            this.toolBar.TabIndex = 2;
            this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
            // 
            // backButton
            // 
            this.backButton.DropDownMenu = this.backButtonMenu;
            this.backButton.ImageIndex = 0;
            this.backButton.Name = "backButton";
            this.backButton.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton;
            // 
            // backButtonMenu
            // 
            this.backButtonMenu.Popup += new System.EventHandler(this.backButton_Popup);
            // 
            // forwardButton
            // 
            this.forwardButton.DropDownMenu = this.forwardButtonMenu;
            this.forwardButton.ImageIndex = 1;
            this.forwardButton.Name = "forwardButton";
            this.forwardButton.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton;
            // 
            // forwardButtonMenu
            // 
            this.forwardButtonMenu.Popup += new System.EventHandler(this.forwardButton_Popup);
            // 
            // upButton
            // 
            this.upButton.ImageIndex = 2;
            this.upButton.Name = "upButton";
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList.Images.SetKeyName(0, "Back.bmp");
            this.imageList.Images.SetKeyName(1, "Forward.bmp");
            this.imageList.Images.SetKeyName(2, "Up.bmp");
            // 
            // fileDialogToolbar1
            // 
            this.fileDialogToolbar1.AutoSize = true;
            this.fileDialogToolbar1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileDialogToolbar1.Location = new System.Drawing.Point(0, 0);
            this.fileDialogToolbar1.Name = "fileDialogToolbar1";
            this.fileDialogToolbar1.ShellView = this.shellView;
            this.fileDialogToolbar1.Size = new System.Drawing.Size(780, 29);
            this.fileDialogToolbar1.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.splitContainer2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer1.Size = new System.Drawing.Size(886, 518);
            this.splitContainer1.SplitterDistance = 29;
            this.splitContainer1.TabIndex = 10;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.toolBar);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.fileDialogToolbar1);
            this.splitContainer2.Size = new System.Drawing.Size(886, 29);
            this.splitContainer2.SplitterDistance = 103;
            this.splitContainer2.SplitterWidth = 3;
            this.splitContainer2.TabIndex = 11;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.splitContainer4);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.shellView);
            this.splitContainer3.Size = new System.Drawing.Size(886, 485);
            this.splitContainer3.SplitterDistance = 346;
            this.splitContainer3.SplitterWidth = 3;
            this.splitContainer3.TabIndex = 0;
            // 
            // splitContainer4
            // 
            this.splitContainer4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer4.Location = new System.Drawing.Point(0, 0);
            this.splitContainer4.Name = "splitContainer4";
            // 
            // splitContainer4.Panel1
            // 
            this.splitContainer4.Panel1.Controls.Add(this.placesToolbar1);
            // 
            // splitContainer4.Panel2
            // 
            this.splitContainer4.Panel2.Controls.Add(this.treeView);
            this.splitContainer4.Size = new System.Drawing.Size(346, 485);
            this.splitContainer4.SplitterDistance = 76;
            this.splitContainer4.SplitterWidth = 3;
            this.splitContainer4.TabIndex = 0;
            // 
            // ShellExplorer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(886, 543);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.statusBar);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Menu = this.mainMenu;
            this.Name = "ShellExplorer";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Shell Explorer V1.0";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
            this.splitContainer3.ResumeLayout(false);
            this.splitContainer4.Panel1.ResumeLayout(false);
            this.splitContainer4.Panel1.PerformLayout();
            this.splitContainer4.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer4)).EndInit();
            this.splitContainer4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ShellTreeView treeView;
        private ShellView shellView;
        private System.Windows.Forms.StatusBar statusBar;
        private System.Windows.Forms.MainMenu mainMenu;
        private System.Windows.Forms.MenuItem fileMenu;
        private System.Windows.Forms.ToolBar toolBar;
        private System.Windows.Forms.ToolBarButton backButton;
        private System.Windows.Forms.ToolBarButton forwardButton;
        private System.Windows.Forms.ToolBarButton upButton;
        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ContextMenu backButtonMenu;
        private System.Windows.Forms.ContextMenu forwardButtonMenu;
        private System.Windows.Forms.MenuItem dummyMenuItem;
        private System.Windows.Forms.MenuItem viewMenu;
        private System.Windows.Forms.MenuItem refreshMenu;
        private PlacesToolbar placesToolbar1;
        private FileDialogToolbar fileDialogToolbar1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.SplitContainer splitContainer4;
    }
}