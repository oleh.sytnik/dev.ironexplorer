using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;

namespace IonShard.Shell.Core
{
    internal class ShellItemEditor : UITypeEditor
    {
        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }

        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
        {
            var f = new ShellItemBrowseForm();

            return f.ShowDialog() == DialogResult.OK ? f.SelectedItem : value;
        }
    }
}