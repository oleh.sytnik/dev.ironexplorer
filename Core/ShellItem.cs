using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using IonShard.Shell.Interop;
using ComTypes = System.Runtime.InteropServices.ComTypes;
using IDropTarget = IonShard.Shell.Interop.IDropTarget;

namespace IonShard.Shell.Core
{
    [TypeConverter(typeof (ShellItemConverter))]
    public class ShellItem : IEnumerable<ShellItem>
    {
        public ShellItem(Uri uri)
        {
            Initialize(uri);
        }
        public ShellItem(string path)
        {
            Initialize(new Uri(path));
        }
        public ShellItem(Environment.SpecialFolder folder)
        {
            IntPtr pidl;

            if (Shell32.SHGetSpecialFolderLocation(IntPtr.Zero,
                (CSIDL) folder, out pidl) == HResult.S_OK)
            {
                try
                {
                    ComInterface = CreateItemFromIDList(pidl);
                }
                finally
                {
                    Shell32.ILFree(pidl);
                }
            }
            else
            {
                var path = new StringBuilder();
                Marshal.ThrowExceptionForHR((int) Shell32.SHGetFolderPath(
                    IntPtr.Zero, (CSIDL) folder, IntPtr.Zero, 0, path));
                ComInterface = CreateItemFromParsingName(path.ToString());
            }
        }
        public ShellItem(ShellItem parent, string name)
        {
            if (parent.IsFileSystem)
            {
                ComInterface = CreateItemFromParsingName(
                    Path.Combine(parent.FileSystemPath, name));
            }
            else
            {
                var folder = parent.GetIShellFolder();
                uint eaten;
                IntPtr pidl;
                uint attributes = 0;

                folder.ParseDisplayName(IntPtr.Zero, IntPtr.Zero,
                    name, out eaten, out pidl, ref attributes);

                try
                {
                    ComInterface = CreateItemFromIDList(pidl);
                }
                finally
                {
                    Shell32.ILFree(pidl);
                }
            }
        }

        internal ShellItem(IntPtr pidl)
        {
            ComInterface = CreateItemFromIDList(pidl);
        }

        internal ShellItem(ShellItem parent, IntPtr pidl)
        {
            ComInterface = CreateItemWithParent(parent, pidl);
        }

        public ShellItem(IShellItem comInterface)
        {
            ComInterface = comInterface;
        }

        public int Compare(ShellItem item)
        {
            var result = ComInterface.Compare(item.ComInterface,
                SICHINT.DISPLAY);
            return result;
        }
        public override bool Equals(object obj)
        {
            if (!(obj is ShellItem)) return false;
            var otherItem = (ShellItem) obj;
            var result = ComInterface.Compare(otherItem.ComInterface,
                SICHINT.DISPLAY) == 0;
            if (!result)
            {
                result = IsFileSystem && otherItem.IsFileSystem &&
                         FileSystemPath == otherItem.FileSystemPath;
            }

            return result;
        }
        public string GetDisplayName(SIGDN sigdn)
        {
            var resultPtr = ComInterface.GetDisplayName(sigdn);
            var result = Marshal.PtrToStringUni(resultPtr);
            Marshal.FreeCoTaskMem(resultPtr);
            return result;
        }

        public IEnumerator<ShellItem> GetEnumerator()
        {
            return GetEnumerator(SHCONTF.FOLDERS | SHCONTF.INCLUDEHIDDEN |
                                 SHCONTF.NONFOLDERS);
        }

        public IEnumerator<ShellItem> GetEnumerator(SHCONTF filter)
        {
            var folder = GetIShellFolder();
            var enumId = GetIEnumIDList(folder, filter);
            uint count;
            IntPtr pidl;

            if (enumId == null)
            {
                yield break;
            }

            var result = enumId.Next(1, out pidl, out count);
            while (result == HResult.S_OK)
            {
                yield return new ShellItem(this, pidl);
                Shell32.ILFree(pidl);
                result = enumId.Next(1, out pidl, out count);
            }

            if (result != HResult.S_FALSE)
            {
                Marshal.ThrowExceptionForHR((int) result);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public ComTypes.IDataObject GetIDataObject()
        {
            var result = ComInterface.BindToHandler(IntPtr.Zero,
                BHID.SFUIObject, typeof (ComTypes.IDataObject).GUID);
            return (ComTypes.IDataObject) Marshal.GetTypedObjectForIUnknown(result,
                typeof (ComTypes.IDataObject));
        }

        public IDropTarget GetIDropTarget(Control control)
        {
            var result = GetIShellFolder().CreateViewObject(control.Handle,
                typeof (IDropTarget).GUID);
            return (IDropTarget) Marshal.GetTypedObjectForIUnknown(result,
                typeof (IDropTarget));
        }

        public IShellFolder GetIShellFolder()
        {
            var result = ComInterface.BindToHandler(IntPtr.Zero,
                BHID.SFObject, typeof (IShellFolder).GUID);
            return (IShellFolder) Marshal.GetTypedObjectForIUnknown(result,
                typeof (IShellFolder));
        }

        public int GetSystemImageListIndex(ShellIconType type, ShellIconFlags flags)
        {
            var info = new SHFILEINFO();
            var result = Shell32.SHGetFileInfo(Pidl, 0, out info,
                Marshal.SizeOf(info),
                SHGFI.ICON | SHGFI.SYSICONINDEX | SHGFI.OVERLAYINDEX | SHGFI.PIDL |
                (SHGFI) type | (SHGFI) flags);

            if (result == IntPtr.Zero)
            {
                throw new Exception("Error retreiving shell folder icon");
            }

            return info.iIcon;
        }

        public bool IsImmediateParentOf(ShellItem item)
        {
            return IsFolder && Shell32.ILIsParent(Pidl, item.Pidl, true);
        }

        public bool IsParentOf(ShellItem item)
        {
            return IsFolder && Shell32.ILIsParent(Pidl, item.Pidl, false);
        }

        public override string ToString()
        {
            return ToUri().ToString();
        }

        public Uri ToUri()
        {
            var manager = new KnownFolderManager();
            var path = new StringBuilder("shell:///");
            var knownFolder = manager.FindNearestParent(this);

            if (knownFolder == null) return new Uri(FileSystemPath);
            var folders = new List<string>();
            var knownFolderItem = knownFolder.CreateShellItem();
            var item = this;

            while (item != knownFolderItem)
            {
                folders.Add(item.GetDisplayName(SIGDN.PARENTRELATIVEPARSING));
                item = item.Parent;
            }

            folders.Reverse();
            path.Append(knownFolder.Name);
            foreach (var s in folders)
            {
                path.Append('/');
                path.Append(s);
            }

            return new Uri(path.ToString());
        }

        public ShellItem this[string name]
        {
            get { return new ShellItem(this, name); }
        }

        public static bool operator !=(ShellItem a, ShellItem b)
        {
            if (ReferenceEquals(a, null))
            {
                return !ReferenceEquals(b, null);
            }
            return !a.Equals(b);
        }

        public static bool operator ==(ShellItem a, ShellItem b)
        {
            return ReferenceEquals(a, null) ? ReferenceEquals(b, null) : a.Equals(b);
        }

        public IShellItem ComInterface { get; private set; }

        public string DisplayName
        {
            get { return GetDisplayName(SIGDN.NORMALDISPLAY); }
        }

        public string FileSystemPath
        {
            get { return GetDisplayName(SIGDN.FILESYSPATH); }
        }

        public bool HasSubFolders
        {
            get { return ComInterface.GetAttributes(SFGAO.HASSUBFOLDER) != 0; }
        }

        public bool IsFileSystem
        {
            get { return ComInterface.GetAttributes(SFGAO.FILESYSTEM) != 0; }
        }

        public bool IsFileSystemAncestor
        {
            get { return ComInterface.GetAttributes(SFGAO.FILESYSANCESTOR) != 0; }
        }

        public bool IsFolder
        {
            get { return ComInterface.GetAttributes(SFGAO.FOLDER) != 0; }
        }

        public bool IsReadOnly
        {
            get { return ComInterface.GetAttributes(SFGAO.READONLY) != 0; }
        }

        public ShellItem Parent
        {
            get
            {
                IShellItem item;
                var result = ComInterface.GetParent(out item);

                switch (result)
                {
                    case HResult.S_OK:
                        return new ShellItem(item);
                    case HResult.MK_E_NOOBJECT:
                        return null;
                    case HResult.DRAGDROP_S_CANCEL:
                        break;
                    case HResult.DRAGDROP_S_DROP:
                        break;
                    case HResult.DRAGDROP_S_USEDEFAULTCURSORS:
                        break;
                    case HResult.DATA_S_SAMEFORMATETC:
                        break;
                    case HResult.S_FALSE:
                        break;
                    case HResult.E_NOINTERFACE:
                        break;
                    case HResult.E_NOTIMPL:
                        break;
                    case HResult.OLE_E_ADVISENOTSUPPORTED:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                Marshal.ThrowExceptionForHR((int) result);
                return null;
            }
        }

        public string ParsingName
        {
            get { return GetDisplayName(SIGDN.DESKTOPABSOLUTEPARSING); }
        }

        public IntPtr Pidl
        {
            get { return GetIDListFromObject(ComInterface); }
        }

        public Icon ShellIcon
        {
            get
            {
                var info = new SHFILEINFO();
                var result = Shell32.SHGetFileInfo(Pidl, 0, out info,
                    Marshal.SizeOf(info),
                    SHGFI.ADDOVERLAYS | SHGFI.ICON |
                    SHGFI.SHELLICONSIZE | SHGFI.PIDL);

                if (result == IntPtr.Zero)
                {
                    throw new Exception("Error retreiving shell folder icon");
                }

                return Icon.FromHandle(info.hIcon);
            }
        }

        public string ToolTipText
        {
            get
            {
                IntPtr result;
                IntPtr infoTipPtr;

                try
                {
                    var relativePidl = Shell32.ILFindLastID(Pidl);
                    Parent.GetIShellFolder().GetUIObjectOf(IntPtr.Zero, 1,
                        new[] {relativePidl},
                        typeof (IQueryInfo).GUID, 0, out result);
                }
                catch (Exception)
                {
                    return string.Empty;
                }

                var queryInfo = (IQueryInfo)
                    Marshal.GetTypedObjectForIUnknown(result,
                        typeof (IQueryInfo));
                queryInfo.GetInfoTip(0, out infoTipPtr);
                var infoTip = Marshal.PtrToStringUni(infoTipPtr);
                Ole32.CoTaskMemFree(infoTipPtr);
                return infoTip;
            }
        }

        public static ShellItem Desktop
        {
            get
            {
                if (m_Desktop != null) return m_Desktop;
                IShellItem item;
                IntPtr pidl;

                Shell32.SHGetSpecialFolderLocation(
                    IntPtr.Zero, (CSIDL) Environment.SpecialFolder.Desktop,
                    out pidl);

                try
                {
                    item = CreateItemFromIDList(pidl);
                }
                finally
                {
                    Shell32.ILFree(pidl);
                }

                m_Desktop = new ShellItem(item);
                return m_Desktop;
            }
        }

        internal static bool RunningVista
        {
            get { return Environment.OSVersion.Version.Major >= 6; }
        }

        private void Initialize(Uri uri)
        {
            switch (uri.Scheme)
            {
                case "file":
                    ComInterface = CreateItemFromParsingName(uri.LocalPath);
                    break;
                case "shell":
                    InitializeFromShellUri(uri);
                    break;
                default:
                    throw new InvalidOperationException("Invalid uri scheme");
            }
        }

        private void InitializeFromShellUri(Uri uri)
        {
            var manager = new KnownFolderManager();
            var path = uri.GetComponents(UriComponents.Path, UriFormat.Unescaped);
            string knownFolder;
            string restOfPath;
            var separatorIndex = path.IndexOf('/');

            if (separatorIndex != -1)
            {
                knownFolder = path.Substring(0, separatorIndex);
                restOfPath = path.Substring(separatorIndex + 1);
            }
            else
            {
                knownFolder = path;
                restOfPath = string.Empty;
            }

            ComInterface = manager.GetFolder(knownFolder).CreateShellItem().ComInterface;

            if (restOfPath != string.Empty)
            {
                ComInterface = this[restOfPath.Replace('/', '\\')].ComInterface;
            }
        }

        private static IShellItem CreateItemFromIDList(IntPtr pidl)
        {
            if (RunningVista)
            {
                return Shell32.SHCreateItemFromIDList(pidl,
                    typeof (IShellItem).GUID);
            }
            return new ShellItemImpl(
                pidl, false);
        }

        private static IShellItem CreateItemFromParsingName(string path)
        {
            if (RunningVista)
            {
                return Shell32.SHCreateItemFromParsingName(path, IntPtr.Zero,
                    typeof (IShellItem).GUID);
            }
            var desktop = Desktop.GetIShellFolder();
            uint attributes = 0;
            uint eaten;
            IntPtr pidl;

            desktop.ParseDisplayName(IntPtr.Zero, IntPtr.Zero,
                path, out eaten, out pidl, ref attributes);
            return new ShellItemImpl(
                pidl, true);
        }

        private static IShellItem CreateItemWithParent(ShellItem parent, IntPtr pidl)
        {
            if (RunningVista)
            {
                return Shell32.SHCreateItemWithParent(IntPtr.Zero,
                    parent.GetIShellFolder(), pidl, typeof (IShellItem).GUID);
            }
            var impl =
                (ShellItemImpl) parent.ComInterface;
            return new ShellItemImpl(
                Shell32.ILCombine(impl.Pidl, pidl), true);
        }

        private static IntPtr GetIDListFromObject(IShellItem item)
        {
            return RunningVista ? Shell32.SHGetIDListFromObject(item) : ((ShellItemImpl) item).Pidl;
        }

        private static IEnumIDList GetIEnumIDList(IShellFolder folder, SHCONTF flags)
        {
            IEnumIDList result;

            return folder.EnumObjects(IntPtr.Zero, flags, out result) == HResult.S_OK ? result : null;
        }

        private static ShellItem m_Desktop;
    }

    internal class ShellItemConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context,
            Type sourceType)
        {
            return sourceType == typeof (string) || base.CanConvertFrom(context, sourceType);
        }

        public override bool CanConvertTo(ITypeDescriptorContext context,
            Type destinationType)
        {
            return destinationType == typeof (InstanceDescriptor) || base.CanConvertTo(context, destinationType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture,
            object value)
        {
            if (!(value is string)) return base.ConvertFrom(context, culture, value);
            var s = (string) value;

            return s.Length == 0 ? ShellItem.Desktop : new ShellItem(s);
        }

        public override object ConvertTo(ITypeDescriptorContext context,
            CultureInfo culture,
            object value,
            Type destinationType)
        {
            if (!(value is ShellItem)) return base.ConvertTo(context, culture, value, destinationType);
            var uri = ((ShellItem) value).ToUri();

            if (destinationType == typeof (string))
            {
                return uri.Scheme == "file" ? uri.LocalPath : uri.ToString();
            }
            if (destinationType == typeof (InstanceDescriptor))
            {
                return new InstanceDescriptor(
                    typeof (ShellItem).GetConstructor(new[] {typeof (string)}),
                    new object[] {uri.ToString()});
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }
    }

    public enum ShellIconType
    {
        LargeIcon = SHGFI.LARGEICON,
        ShellIcon = SHGFI.SHELLICONSIZE,
        SmallIcon = SHGFI.SMALLICON,
    }

    [Flags]
    public enum ShellIconFlags
    {
        OpenIcon = SHGFI.OPENICON,
        OverlayIndex = SHGFI.OVERLAYINDEX
    }
}