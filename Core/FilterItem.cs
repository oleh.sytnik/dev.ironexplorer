using System;
using System.Collections.Generic;
using System.Linq;

namespace IonShard.Shell.Core
{
    internal class FilterItem
    {
        public FilterItem(string caption, string filter)
        {
            Caption = caption;
            Filter = filter;
        }

        public bool Contains(string filter)
        {
            var filters = Filter.Split(',');

            return filters.Any(s => filter == s.Trim());
        }

        public override string ToString()
        {
            var filterString = string.Format(" ({0})", Filter);

            if (Caption.EndsWith(filterString))
            {
                return Caption;
            }
            return Caption + filterString;
        }

        public static FilterItem[] ParseFilterString(string filterString)
        {
            int dummy;
            return ParseFilterString(filterString, string.Empty, out dummy);
        }

        public static FilterItem[] ParseFilterString(string filterString,
            string existing,
            out int existingIndex)
        {
            var result = new List<FilterItem>();

            existingIndex = -1;

            var items = filterString != string.Empty ? filterString.Split('|') : new string[0];

            if (items.Length % 2 != 0)
            {
                throw new ArgumentException(
                    "Filter string you provided is not valid. The filter " +
                    "string must contain a description of the filter, " +
                    "followed by the vertical bar (|) and the filter pattern." +
                    "The strings for different filtering options must also be " +
                    "separated by the vertical bar. Example: " +
                    "\"Text files|*.txt|All files|*.*\"");
            }

            for (var n = 0; n < items.Length; n += 2)
            {
                var item = new FilterItem(items[n], items[n + 1]);
                result.Add(item);
                if (item.Filter == existing) existingIndex = result.Count - 1;
            }

            return result.ToArray();
        }

        public string Caption;
        public string Filter;
    }
}