using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IonShard.Shell.Interop;

namespace IonShard.Shell.Core
{
    public class ShellContextMenu
    {
        public ShellContextMenu(ShellItem item)
        {
            Initialize(new[] {item});
        }

        public ShellContextMenu(ShellItem[] items)
        {
            Initialize(items);
        }

        public bool HandleMenuMessage(ref Message m)
        {
            if (m.Msg == (int) MSG.WM_COMMAND && (int) m.WParam >= m_CmdFirst)
            {
                InvokeCommand((int) m.WParam - m_CmdFirst);
                return true;
            }
            if (m_ComInterface3 != null)
            {
                IntPtr result;
                if (m_ComInterface3.HandleMenuMsg2(m.Msg, m.WParam, m.LParam,
                        out result) != HResult.S_OK) return false;
                m.Result = result;
                return true;
            }
            if (m_ComInterface2 == null) return false;
            if (m_ComInterface2.HandleMenuMsg(m.Msg, m.WParam, m.LParam) != HResult.S_OK) return false;
            m.Result = IntPtr.Zero;
            return true;
        }

        public void InvokeDelete()
        {
            var invoke = new CMINVOKECOMMANDINFO();
            invoke.cbSize = Marshal.SizeOf(invoke);
            invoke.lpVerb = "delete";

            try
            {
                ComInterface.InvokeCommand(ref invoke);
            }
            catch (COMException e)
            {
                if (e.ErrorCode != unchecked((int) 0x800704C7)) throw;
            }
        }

        public void InvokeRename()
        {
            var invoke = new CMINVOKECOMMANDINFO();
            invoke.cbSize = Marshal.SizeOf(invoke);
            invoke.lpVerb = "rename";
            ComInterface.InvokeCommand(ref invoke);
        }

        public void Populate(Menu menu)
        {
            RemoveShellMenuItems(menu);
            ComInterface.QueryContextMenu(menu.Handle, 0,
                m_CmdFirst, int.MaxValue, CMF.EXPLORE);
        }

        public void ShowContextMenu(Control control, Point pos)
        {
            using (var menu = new ContextMenu())
            {
                pos = control.PointToScreen(pos);
                Populate(menu);
                var command = User32.TrackPopupMenuEx(menu.Handle,
                    TPM.TPM_RETURNCMD, pos.X, pos.Y, m_MessageWindow.Handle,
                    IntPtr.Zero);
                if (command > 0)
                {
                    InvokeCommand(command - m_CmdFirst);
                }
            }
        }

        public IContextMenu ComInterface { get; set; }

        private void Initialize(IReadOnlyList<ShellItem> items)
        {
            var pidls = new IntPtr[items.Count];
            ShellItem parent = null;
            IntPtr result;

            for (var n = 0; n < items.Count; ++n)
            {
                pidls[n] = Shell32.ILFindLastID(items[n].Pidl);

                if (parent == null)
                {
                    parent = items[n] == ShellItem.Desktop ? ShellItem.Desktop : items[n].Parent;
                }
                else
                {
                    if (items[n].Parent != parent)
                    {
                        throw new Exception("All shell items must have the same parent");
                    }
                }
            }

            parent.GetIShellFolder().GetUIObjectOf(IntPtr.Zero,
                (uint) pidls.Length, pidls,
                typeof(IContextMenu).GUID, 0, out result);
            ComInterface = (IContextMenu)
                Marshal.GetTypedObjectForIUnknown(result,
                    typeof(IContextMenu));
            m_ComInterface2 = ComInterface as IContextMenu2;
            m_ComInterface3 = ComInterface as IContextMenu3;
            m_MessageWindow = new MessageWindow(this);
        }

        private void InvokeCommand(int index)
        {
            const int SW_SHOWNORMAL = 1;
            var invoke = new CMINVOKECOMMANDINFO_ByIndex();
            invoke.cbSize = Marshal.SizeOf(invoke);
            invoke.iVerb = index;
            invoke.nShow = SW_SHOWNORMAL;
            m_ComInterface2.InvokeCommand(ref invoke);
        }

        private void TagManagedMenuItems(Menu menu, int tag)
        {
            var info = new MENUINFO();

            info.cbSize = Marshal.SizeOf(info);
            info.fMask = MIM.MIM_MENUDATA;
            info.dwMenuData = tag;

            foreach (MenuItem item in menu.MenuItems)
            {
                User32.SetMenuInfo(item.Handle, ref info);
            }
        }

        private void RemoveShellMenuItems(Menu menu)
        {
            const int tag = 0xAB;
            var remove = new List<int>();
            var count = User32.GetMenuItemCount(menu.Handle);
            var menuInfo = new MENUINFO();
            var itemInfo = new MENUITEMINFO();

            menuInfo.cbSize = Marshal.SizeOf(menuInfo);
            menuInfo.fMask = MIM.MIM_MENUDATA;
            itemInfo.cbSize = Marshal.SizeOf(itemInfo);
            itemInfo.fMask = MIIM.MIIM_ID | MIIM.MIIM_SUBMENU;

            TagManagedMenuItems(menu, tag);

            for (var n = 0; n < count; ++n)
            {
                User32.GetMenuItemInfo(menu.Handle, n, true, ref itemInfo);

                if (itemInfo.hSubMenu == IntPtr.Zero)
                {
                    if (itemInfo.wID >= m_CmdFirst) remove.Add(n);
                }
                else
                {
                    User32.GetMenuInfo(itemInfo.hSubMenu, ref menuInfo);
                    if (menuInfo.dwMenuData != tag) remove.Add(n);
                }
            }
            remove.Reverse();
            foreach (var position in remove)
            {
                User32.DeleteMenu(menu.Handle, position, MF.MF_BYPOSITION);
            }
        }

        private class MessageWindow : Control
        {
            public MessageWindow(ShellContextMenu parent)
            {
                m_Parent = parent;
            }

            protected override void WndProc(ref Message m)
            {
                if (!m_Parent.HandleMenuMessage(ref m))
                {
                    base.WndProc(ref m);
                }
            }

            private ShellContextMenu m_Parent;
        }

        private MessageWindow m_MessageWindow;
        private IContextMenu2 m_ComInterface2;
        private IContextMenu3 m_ComInterface3;
        private const int m_CmdFirst = 0x8000;
    }
}