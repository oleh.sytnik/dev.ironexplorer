using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using IonShard.Shell.Interop;

namespace IonShard.Shell.Core
{
    public class KnownFolderManager : IEnumerable<KnownFolder>
    {
        public KnownFolderManager()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            {
                m_ComInterface = (IKnownFolderManager)
                    new CoClass.KnownFolderManager();
            }
            else
            {
                m_NameIndex = new Dictionary<string, KnownFolder>();
                m_PathIndex = new Dictionary<string, KnownFolder>();

                AddFolder("Common Desktop", CSIDL.COMMON_DESKTOPDIRECTORY);
                AddFolder("Desktop", CSIDL.DESKTOP);
                AddFolder("Personal", CSIDL.PERSONAL);
                AddFolder("Recent", CSIDL.RECENT);
                AddFolder("MyComputerFolder", CSIDL.DRIVES);
                AddFolder("My Pictures", CSIDL.MYPICTURES);
                AddFolder("ProgramFilesCommon", CSIDL.PROGRAM_FILES_COMMON);
                AddFolder("Windows", CSIDL.WINDOWS);
            }
        }

        public KnownFolder FindNearestParent(ShellItem item)
        {
            if (m_ComInterface != null)
            {
                IKnownFolder iKnownFolder;

                if (item.IsFileSystem)
                {
                    if (m_ComInterface.FindFolderFromPath(item.FileSystemPath,
                            FFFP_MODE.NEARESTPARENTMATCH, out iKnownFolder)
                        == HResult.S_OK)
                    {
                        return CreateFolder(iKnownFolder);
                    }
                }
                else
                {
                    if (m_ComInterface.FindFolderFromIDList(item.Pidl, out iKnownFolder)
                        == HResult.S_OK)
                    {
                        return CreateFolder(iKnownFolder);
                    }
                }
            }
            else
            {
                return item.IsFileSystem
                    ? (from i in m_PathIndex
                        where i.Key != string.Empty && item.FileSystemPath.StartsWith(i.Key)
                        select i.Value).FirstOrDefault()
                    : (from i in m_NameIndex where item == i.Value.CreateShellItem() select i.Value).FirstOrDefault();
            }

            return null;
        }

        public IEnumerator<KnownFolder> GetEnumerator()
        {
            if (m_ComInterface != null)
            {
                IntPtr buffer;
                uint count;
                m_ComInterface.GetFolderIds(out buffer, out count);

                KnownFolder[] results;
                try
                {
                    results = new KnownFolder[count];
                    var p = buffer;

                    for (uint n = 0; n < count; ++n)
                    {
                        var guid = (Guid) Marshal.PtrToStructure(p, typeof(Guid));
                        results[n] = GetFolder(guid);
                        p = (IntPtr) ((int) p + Marshal.SizeOf(typeof(Guid)));
                    }
                }
                finally
                {
                    Marshal.FreeCoTaskMem(buffer);
                }

                foreach (var f in results)
                {
                    yield return f;
                }
            }
            else
            {
                foreach (var f in m_NameIndex.Values)
                {
                    yield return f;
                }
            }
        }

        public KnownFolder GetFolder(Guid guid)
        {
            return CreateFolder(m_ComInterface.GetFolder(guid));
        }

        public KnownFolder GetFolder(string name)
        {
            if (m_ComInterface != null)
            {
                IKnownFolder iKnownFolder;

                if (m_ComInterface.GetFolderByName(name, out iKnownFolder)
                    == HResult.S_OK)
                {
                    return CreateFolder(iKnownFolder);
                }
            }
            else
            {
                return m_NameIndex[name];
            }

            throw new InvalidOperationException("Unknown shell folder: " + name);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<KnownFolder>) this).GetEnumerator();
        }

        private void AddFolder(string name, CSIDL csidl)
        {
            var folder = CreateFolder(csidl, name);

            m_NameIndex.Add(folder.Name, folder);

            if (folder.ParsingName != string.Empty)
            {
                m_PathIndex.Add(folder.ParsingName, folder);
            }
        }

        private static KnownFolder CreateFolder(CSIDL csidl, string name)
        {
            var path = new StringBuilder(512);

            return Shell32.SHGetFolderPath(IntPtr.Zero, csidl, IntPtr.Zero, 0, path) == HResult.S_OK
                ? new KnownFolder(csidl, name, path.ToString())
                : new KnownFolder(csidl, name, string.Empty);
        }

        private static KnownFolder CreateFolder(IKnownFolder iface)
        {
            var def = iface.GetFolderDefinition();

            try
            {
                return new KnownFolder(iface,
                    Marshal.PtrToStringUni(def.pszName),
                    Marshal.PtrToStringUni(def.pszParsingName));
            }
            finally
            {
                Marshal.FreeCoTaskMem(def.pszName);
                Marshal.FreeCoTaskMem(def.pszDescription);
                Marshal.FreeCoTaskMem(def.pszRelativePath);
                Marshal.FreeCoTaskMem(def.pszParsingName);
                Marshal.FreeCoTaskMem(def.pszTooltip);
                Marshal.FreeCoTaskMem(def.pszLocalizedName);
                Marshal.FreeCoTaskMem(def.pszIcon);
                Marshal.FreeCoTaskMem(def.pszSecurity);
            }
        }

        private IKnownFolderManager m_ComInterface;
        private Dictionary<string, KnownFolder> m_NameIndex;
        private Dictionary<string, KnownFolder> m_PathIndex;
    }

    public class KnownFolder
    {
        public KnownFolder(IKnownFolder iface, string name, string parsingName)
        {
            m_ComInterface = iface;
            Name = name;
            ParsingName = parsingName;
        }

        public KnownFolder(CSIDL csidl, string name, string parsingName)
        {
            m_Csidl = csidl;
            Name = name;
            ParsingName = parsingName;
        }

        public ShellItem CreateShellItem()
        {
            if (m_ComInterface != null)
            {
                return new ShellItem(m_ComInterface.GetShellItem(0,
                    typeof(IShellItem).GUID));
            }
            return new ShellItem((Environment.SpecialFolder) m_Csidl);
        }

        public string Name { get; private set; }

        public string ParsingName { get; private set; }

        private IKnownFolder m_ComInterface;
        private CSIDL m_Csidl;
    }
}