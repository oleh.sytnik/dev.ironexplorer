using System;
using System.Runtime.InteropServices;
using System.Text;

namespace IonShard.Shell.Interop
{
    internal class ShellItemImpl : IDisposable, IShellItem
    {
        public ShellItemImpl(IntPtr pidl, bool owner)
        {
            Pidl = owner ? pidl : Shell32.ILClone(pidl);
        }

        ~ShellItemImpl()
        {
            Dispose(false);
        }

        public IntPtr BindToHandler(IntPtr pbc, Guid bhid, Guid riid)
        {
            if (riid == typeof(IShellFolder).GUID)
            {
                return Marshal.GetIUnknownForObject(GetIShellFolder());
            }
            throw new InvalidCastException();
        }

        public void Dispose()
        {
            Dispose(true);
        }

        public HResult GetParent(out IShellItem ppsi)
        {
            var pidl = Shell32.ILClone(Pidl);
            if (Shell32.ILRemoveLastID(pidl))
            {
                ppsi = new ShellItemImpl(pidl, true);
                return HResult.S_OK;
            }
            ppsi = null;
            return HResult.MK_E_NOOBJECT;
        }

        public IntPtr GetDisplayName(SIGDN sigdnName)
        {
            if (sigdnName == SIGDN.FILESYSPATH)
            {
                var result = new StringBuilder(512);
                if (!Shell32.SHGetPathFromIDList(Pidl, result))
                    throw new ArgumentException();
                return Marshal.StringToHGlobalUni(result.ToString());
            }
            var parentFolder = GetParent().GetIShellFolder();
            var childPidl = Shell32.ILFindLastID(Pidl);
            var builder = new StringBuilder(512);
            var strret = new STRRET();
            parentFolder.GetDisplayNameOf(childPidl,
                (SHGNO) ((int) sigdnName & 0xffff), out strret);
            ShlWapi.StrRetToBuf(ref strret, childPidl, builder,
                (uint) builder.Capacity);
            return Marshal.StringToHGlobalUni(builder.ToString());
        }

        public SFGAO GetAttributes(SFGAO sfgaoMask)
        {
            var parentFolder = GetParent().GetIShellFolder();
            var result = sfgaoMask;

            parentFolder.GetAttributesOf(1,
                new[] {Shell32.ILFindLastID(Pidl)},
                ref result);
            return result & sfgaoMask;
        }

        public int Compare(IShellItem psi, SICHINT hint)
        {
            var other = (ShellItemImpl) psi;
            var myParent = GetParent();
            var theirParent = other.GetParent();

            if (Shell32.ILIsEqual(myParent.Pidl, theirParent.Pidl))
            {
                return myParent.GetIShellFolder().CompareIDs((SHCIDS) hint,
                    Shell32.ILFindLastID(Pidl),
                    Shell32.ILFindLastID(other.Pidl));
            }
            return 1;
        }

        public IntPtr Pidl { get; private set; }

        protected void Dispose(bool dispose)
        {
            Shell32.ILFree(Pidl);
        }

        private ShellItemImpl GetParent()
        {
            var pidl = Shell32.ILClone(Pidl);

            return Shell32.ILRemoveLastID(pidl) ? new ShellItemImpl(pidl, true) : this;
        }

        private IShellFolder GetIShellFolder()
        {
            var desktop = Shell32.SHGetDesktopFolder();
            IntPtr desktopPidl;

            Shell32.SHGetSpecialFolderLocation(IntPtr.Zero, CSIDL.DESKTOP,
                out desktopPidl);

            if (Shell32.ILIsEqual(Pidl, desktopPidl))
            {
                return desktop;
            }
            IntPtr result;
            desktop.BindToObject(Pidl, IntPtr.Zero,
                typeof(IShellFolder).GUID, out result);
            return (IShellFolder) Marshal.GetTypedObjectForIUnknown(result,
                typeof(IShellFolder));
        }
    }
}