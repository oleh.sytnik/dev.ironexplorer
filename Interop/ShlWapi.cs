using System;
using System.Runtime.InteropServices;
using System.Text;

namespace IonShard.Shell.Interop
{
    public class ShlWapi
    {
        [DllImport("shlwapi.dll")]
        public static extern int StrRetToBuf(ref STRRET pstr, IntPtr pidl,
            StringBuilder pszBuf,
            uint cchBuf);
    }
}