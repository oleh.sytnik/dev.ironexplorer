using System;
using System.Runtime.InteropServices;

namespace IonShard.Shell.Interop
{
    public class Kernel32
    {
        [DllImport("kernel32.dll")]
        public static extern IntPtr GlobalLock(IntPtr hMem);
    }
}