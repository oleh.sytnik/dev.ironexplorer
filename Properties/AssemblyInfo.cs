﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("IronExplorer")]
[assembly: AssemblyDescription("file manager sample")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("KHAI")]
[assembly: AssemblyProduct("IronExplorer")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("IronExplorer")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("a51194d2-1ed9-467b-a9c8-da7af88fdf00")]
[assembly: AssemblyVersion("1.1.1.0")]
[assembly: AssemblyFileVersion("1.1.1.0")]