using System;
using System.Collections.Generic;
using System.Windows.Forms;
using IonShard.Shell.Core;

namespace IonShard.Shell
{
    public partial class ShellExplorer : Form
    {
        public ShellExplorer()
        {
            InitializeComponent();
        }

        private ShellContextMenu _mContextMenu;

        protected override void WndProc(ref Message m)
        {
            if (_mContextMenu == null || !_mContextMenu.HandleMenuMessage(ref m))
            {
                base.WndProc(ref m);
            }
        }

        private void shellView_Navigated(object sender, EventArgs e)
        {
            backButton.Enabled = shellView.CanNavigateBack;
            forwardButton.Enabled = shellView.CanNavigateForward;
            upButton.Enabled = shellView.CanNavigateParent;
        }

        private void fileMenu_Popup(object sender, EventArgs e)
        {
            var selectedItems = shellView.SelectedItems;

            _mContextMenu = selectedItems.Length > 0
                ? new ShellContextMenu(selectedItems)
                : new ShellContextMenu(treeView.SelectedFolder);

            _mContextMenu.Populate(fileMenu);
        }

        private void refreshMenu_Click(object sender, EventArgs e)
        {
            shellView.RefreshContents();
            treeView.RefreshContents();
        }

        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == backButton)
            {
                shellView.NavigateBack();
            }
            else if (e.Button == forwardButton)
            {
                shellView.NavigateForward();
            }
            else if (e.Button == upButton)
            {
                shellView.NavigateParent();
            }
        }

        private void backButton_Popup(object sender, EventArgs e)
        {
            var items = new List<MenuItem>();

            backButtonMenu.MenuItems.Clear();
            foreach (var f in shellView.History.HistoryBack)
            {
                var item = new MenuItem(f.DisplayName) {Tag = f};
                item.Click += backButtonMenuItem_Click;
                items.Insert(0, item);
            }

            backButtonMenu.MenuItems.AddRange(items.ToArray());
        }

        private void forwardButton_Popup(object sender, EventArgs e)
        {
            forwardButtonMenu.MenuItems.Clear();
            foreach (var f in shellView.History.HistoryForward)
            {
                var item = new MenuItem(f.DisplayName) {Tag = f};
                item.Click += forwardButtonMenuItem_Click;
                forwardButtonMenu.MenuItems.Add(item);
            }
        }

        private void backButtonMenuItem_Click(object sender, EventArgs e)
        {
            var item = (MenuItem) sender;
            var folder = (ShellItem) item.Tag;
            shellView.NavigateBack(folder);
        }

        private void forwardButtonMenuItem_Click(object sender, EventArgs e)
        {
            var item = (MenuItem) sender;
            var folder = (ShellItem) item.Tag;
            shellView.NavigateForward(folder);
        }
    }
}