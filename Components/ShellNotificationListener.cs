using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IonShard.Shell.Core;
using IonShard.Shell.Interop;

namespace IonShard.Shell.Components
{
    public class ShellNotificationListener : Component
    {
        public ShellNotificationListener()
        {
            m_Window = new NotificationWindow(this);
        }

        public ShellNotificationListener(IContainer container)
        {
            container.Add(this);
            m_Window = new NotificationWindow(this);
        }

        public event ShellItemEventHandler DriveAdded;
        public event ShellItemEventHandler DriveRemoved;
        public event ShellItemEventHandler FolderCreated;
        public event ShellItemEventHandler FolderDeleted;
        public event ShellItemChangeEventHandler FolderRenamed;
        public event ShellItemEventHandler FolderUpdated;
        public event ShellItemEventHandler ItemCreated;
        public event ShellItemEventHandler ItemDeleted;
        public event ShellItemChangeEventHandler ItemRenamed;
        public event ShellItemEventHandler ItemUpdated;
        public event ShellItemEventHandler SharingChanged;

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            m_Window.Dispose();
        }

        private class NotificationWindow : Control
        {
            public NotificationWindow(ShellNotificationListener parent)
            {
                var notify = new SHChangeNotifyEntry {pidl = ShellItem.Desktop.Pidl, fRecursive = true};
                m_NotifyId = Shell32.SHChangeNotifyRegister(Handle,
                    SHCNRF.InterruptLevel | SHCNRF.ShellLevel,
                    SHCNE.ALLEVENTS, WM_SHNOTIFY, 1, ref notify);
                m_Parent = parent;
            }

            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                Shell32.SHChangeNotifyUnregister(m_NotifyId);
            }

            protected override void WndProc(ref Message m)
            {
                if (m.Msg == WM_SHNOTIFY)
                {
                    var notify = (SHNOTIFYSTRUCT)
                        Marshal.PtrToStructure(m.WParam,
                            typeof(SHNOTIFYSTRUCT));

                    switch ((SHCNE) m.LParam)
                    {
                        case SHCNE.CREATE:
                            if (m_Parent.ItemCreated != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.ItemCreated(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.DELETE:
                            if (m_Parent.ItemDeleted != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.ItemDeleted(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.DRIVEADD:
                            if (m_Parent.DriveAdded != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.DriveAdded(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.DRIVEREMOVED:
                            if (m_Parent.DriveRemoved != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.DriveRemoved(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.MKDIR:
                            if (m_Parent.FolderCreated != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.FolderCreated(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.RMDIR:
                            if (m_Parent.FolderDeleted != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.FolderDeleted(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.UPDATEDIR:
                            if (m_Parent.FolderUpdated != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.FolderUpdated(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.UPDATEITEM:
                            if (m_Parent.ItemUpdated != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.ItemUpdated(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;

                        case SHCNE.RENAMEFOLDER:
                            if (m_Parent.FolderRenamed != null)
                            {
                                var item1 = new ShellItem(notify.dwItem1);
                                var item2 = new ShellItem(notify.dwItem2);
                                m_Parent.FolderRenamed(m_Parent,
                                    new ShellItemChangeEventArgs(item1, item2));
                            }
                            break;

                        case SHCNE.RENAMEITEM:
                            if (m_Parent.ItemRenamed != null)
                            {
                                var item1 = new ShellItem(notify.dwItem1);
                                var item2 = new ShellItem(notify.dwItem2);
                                m_Parent.ItemRenamed(m_Parent,
                                    new ShellItemChangeEventArgs(item1, item2));
                            }
                            break;

                        case SHCNE.NETSHARE:
                        case SHCNE.NETUNSHARE:
                            if (m_Parent.SharingChanged != null)
                            {
                                var item = new ShellItem(notify.dwItem1);
                                m_Parent.SharingChanged(m_Parent,
                                    new ShellItemEventArgs(item));
                            }
                            break;
                        case SHCNE.MEDIAINSERTED:
                            break;
                        case SHCNE.MEDIAREMOVED:
                            break;
                        case SHCNE.ATTRIBUTES:
                            break;
                        case SHCNE.SERVERDISCONNECT:
                            break;
                        case SHCNE.UPDATEIMAGE:
                            break;
                        case SHCNE.DRIVEADDGUI:
                            break;
                        case SHCNE.FREESPACE:
                            break;
                        case SHCNE.EXTENDED_EVENT:
                            break;
                        case SHCNE.ASSOCCHANGED:
                            break;
                        case SHCNE.DISKEVENTS:
                            break;
                        case SHCNE.GLOBALEVENTS:
                            break;
                        case SHCNE.ALLEVENTS:
                            break;
                        case SHCNE.INTERRUPT:
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
                else
                {
                    base.WndProc(ref m);
                }
            }

            private uint m_NotifyId;
            private ShellNotificationListener m_Parent;
            private const int WM_SHNOTIFY = 0x401;
        }

        private NotificationWindow m_Window;
    }

    public class ShellItemEventArgs : EventArgs
    {
        public ShellItemEventArgs(ShellItem item)
        {
            Item = item;
        }

        public ShellItem Item { get; private set; }
    }

    public class ShellItemChangeEventArgs : EventArgs
    {
        public ShellItemChangeEventArgs(ShellItem oldItem,
            ShellItem newItem)
        {
            OldItem = oldItem;
            NewItem = newItem;
        }

        public ShellItem OldItem { get; private set; }

        public ShellItem NewItem { get; private set; }
    }

    public delegate void ShellItemEventHandler(object sender,
        ShellItemEventArgs e);

    public delegate void ShellItemChangeEventHandler(object sender,
        ShellItemChangeEventArgs e);
}