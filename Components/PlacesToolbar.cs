using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using IonShard.Shell.Core;

namespace IonShard.Shell.Components
{
    public partial class PlacesToolbar : UserControl
    {
        public PlacesToolbar()
        {
            InitializeComponent();
            AutoSize = true;
            toolStrip.Renderer = new Renderer();
        }

        public bool Add(ShellItem folder)
        {
            var include = IncludeItem(folder);

            if (!include) return false;
            var button = new ToolStripButton();
            Image image = folder.ShellIcon.ToBitmap();

            toolStrip.ImageScalingSize = image.Size;
            button.AutoSize = false;
            button.Image = image;
            button.Size = new Size(84, image.Height + 35);
            button.Tag = folder;
            button.Text = WrapButtonText(button, folder.DisplayName);
            button.TextImageRelation = TextImageRelation.ImageAboveText;
            button.ToolTipText = folder.ToolTipText;
            button.Click += button_Click;
            toolStrip.Items.Add(button);

            return true;
        }

        [DefaultValue(null)]
        public ShellView ShellView { get; set; }

        public event FilterItemEventHandler FilterItem;

        [Browsable(false)]
        public override bool AllowDrop
        {
            get { return base.AllowDrop; }
            set { base.AllowDrop = value; }
        }

        [Browsable(false)]
        public override bool AutoScroll
        {
            get { return base.AutoScroll; }
            set { base.AutoScroll = value; }
        }

        [Browsable(false)]
        public new Size AutoScrollMargin
        {
            get { return base.AutoScrollMargin; }
            set { base.AutoScrollMargin = value; }
        }

        [Browsable(false)]
        public new Size AutoScrollMinSize
        {
            get { return base.AutoScrollMinSize; }
            set { base.AutoScrollMinSize = value; }
        }

        [Browsable(false), DefaultValue(true)]
        public override bool AutoSize
        {
            get { return base.AutoSize; }
            set { base.AutoSize = value; }
        }

        [Browsable(false), DefaultValue(AutoSizeMode.GrowAndShrink)]
        public new AutoSizeMode AutoSizeMode
        {
            get { return base.AutoSizeMode; }
            set { base.AutoSizeMode = value; }
        }

        [Browsable(false)]
        public override AutoValidate AutoValidate
        {
            get { return base.AutoValidate; }
            set { base.AutoValidate = value; }
        }

        [Browsable(false)]
        public override Image BackgroundImage
        {
            get { return base.BackgroundImage; }
            set { base.BackgroundImage = value; }
        }

        [Browsable(false)]
        public override ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }

        [Browsable(false)]
        public new BorderStyle BorderStyle
        {
            get { return base.BorderStyle; }
            set { base.BorderStyle = value; }
        }

        [Browsable(false)]
        public new bool CausesValidation
        {
            get { return base.CausesValidation; }
            set { base.CausesValidation = value; }
        }

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            if (toolStrip.Items.Count == 0) CreateDefaultItems();
        }

        private void CreateDefaultItems()
        {
            const int CSIDL_NETWORK = 0x0012;
            Add(new ShellItem(Environment.SpecialFolder.Recent));
            Add(new ShellItem(Environment.SpecialFolder.Desktop));
            Add(new ShellItem(Environment.SpecialFolder.Personal));
            Add(new ShellItem(Environment.SpecialFolder.MyComputer));
            Add(new ShellItem((Environment.SpecialFolder) CSIDL_NETWORK));
        }

        private bool IncludeItem(ShellItem item)
        {
            if (FilterItem == null) return true;
            var e = new FilterItemEventArgs(item);
            FilterItem(this, e);
            return e.Include;
        }

        private string WrapButtonText(ToolStripItem button, string s)
        {
            var g = Graphics.FromHwnd(toolStrip.Handle);

            if (!(g.MeasureString(s, button.Font).Width >
                  button.ContentRectangle.Width)) return s;
            var lastSpace = s.LastIndexOf(' ');
            return s.Substring(0, lastSpace) + "\r\n" +
                   s.Substring(lastSpace + 1);
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (ShellView == null) return;
            var folder = (ShellItem) ((ToolStripButton) sender).Tag;
            ShellView.Navigate(folder);
        }

        private class Renderer : ToolStripSystemRenderer
        {
            protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
            {
                var rect = new Rectangle(0, 0,
                    e.ToolStrip.Width - 1, e.ToolStrip.Height - 1);

                if (Application.RenderWithVisualStyles)
                {
                    ControlPaint.DrawVisualStyleBorder(e.Graphics, rect);
                }
                else
                {
                    ControlPaint.DrawBorder3D(e.Graphics, rect,
                        Border3DStyle.Sunken);
                }
            }
        }
    }
}