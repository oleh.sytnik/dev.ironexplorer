using System;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using IonShard.Shell.Core;

namespace IonShard.Shell.Components
{
    public class FileFilterComboBox : ComboBox
    {
        public FileFilterComboBox()
        {
            DropDownStyle = ComboBoxStyle.DropDownList;
            _mRegex = GenerateRegex(_mFilter);
        }

        [Category("Behaviour"), DefaultValue("*.*")]
        public string Filter
        {
            get { return _mFilter; }
            set
            {
                _mFilter = !string.IsNullOrEmpty(value) ? value : "*.*";
                _mRegex = GenerateRegex(_mFilter);
                foreach (var item in Items.Cast<FilterItem>().Where(item => item.Contains(_mFilter)))
                {
                    try
                    {
                        _mIgnoreSelectionChange = true;
                        SelectedItem = item;
                    }
                    finally
                    {
                        _mIgnoreSelectionChange = false;
                    }
                }
                if (_mShellView != null)
                {
                    _mShellView.RefreshContents();
                }
            }
        }

        [Browsable(false)]
        [DefaultValue(ComboBoxStyle.DropDownList)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new ComboBoxStyle DropDownStyle
        {
            get { return base.DropDownStyle; }
            set { base.DropDownStyle = value; }
        }

        [Category("Behaviour")]
        public string FilterItems
        {
            get { return _mFilterItems; }
            set
            {
                int selection;

                Items.Clear();
                Items.AddRange(FilterItem.ParseFilterString(value, _mFilter,
                    out selection));

                if (selection != -1)
                {
                    SelectedIndex = selection;
                }
                else
                {
                    try
                    {
                        _mIgnoreSelectionChange = true;
                        SelectedIndex = Items.Count - 1;
                    }
                    finally
                    {
                        _mIgnoreSelectionChange = false;
                    }
                }

                _mFilterItems = value;
            }
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public new ObjectCollection Items
        {
            get { return base.Items; }
        }

        [DefaultValue(null)]
        [Category("Behaviour")]
        public ShellView ShellView
        {
            get { return _mShellView; }
            set
            {
                if (_mShellView != null)
                {
                    _mShellView.FilterItem -= m_ShellView_FilterItem;
                }

                _mShellView = value;

                if (_mShellView != null)
                {
                    _mShellView.FilterItem += m_ShellView_FilterItem;
                }
            }
        }

        public static Regex GenerateRegex(string wildcard)
        {
            var wildcards = wildcard.Split(',');
            var regexString = new StringBuilder();

            foreach (var s in wildcards)
            {
                if (regexString.Length > 0)
                {
                    regexString.Append('|');
                }

                regexString.Append(
                    Regex.Escape(s).Replace(@"\*", ".*").Replace(@"\?", "."));
            }

            return new Regex(regexString.ToString(),
                RegexOptions.IgnoreCase | RegexOptions.Compiled);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);

            if (_mIgnoreSelectionChange) return;
            if (SelectedItem != null)
            {
                Filter = ((FilterItem) SelectedItem).Filter;
            }
        }

        private void m_ShellView_FilterItem(object sender, FilterItemEventArgs e)
        {
            if (e.Include)
            {
                e.Include = (e.Item.IsFileSystem || e.Item.IsFileSystemAncestor) &&
                            (e.Item.IsFolder || _mRegex.IsMatch(e.Item.FileSystemPath));
            }
        }

        private string _mFilter = "*.*";
        private string _mFilterItems = "";
        private ShellView _mShellView;
        private Regex _mRegex;
        private bool _mIgnoreSelectionChange;
    }
}