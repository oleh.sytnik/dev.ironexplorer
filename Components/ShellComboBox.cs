using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Windows.Forms;
using IonShard.Shell.Core;
using IonShard.Shell.Interop;

namespace IonShard.Shell.Components
{
    public class ShellComboBox : Control
    {
        public ShellComboBox()
        {
            m_Combo.Dock = DockStyle.Fill;
            m_Combo.DrawMode = DrawMode.OwnerDrawFixed;
            m_Combo.DropDownStyle = ComboBoxStyle.DropDownList;
            m_Combo.DropDownHeight = 300;
            m_Combo.ItemHeight = SystemInformation.SmallIconSize.Height + 1;
            m_Combo.Parent = this;
            m_Combo.Click += m_Combo_Click;
            m_Combo.DrawItem += m_Combo_DrawItem;
            m_Combo.SelectedIndexChanged += m_Combo_SelectedIndexChanged;
            m_Edit.Anchor = AnchorStyles.Left | AnchorStyles.Top |
                            AnchorStyles.Right | AnchorStyles.Bottom;
            m_Edit.BorderStyle = BorderStyle.None;
            m_Edit.Left = 8 + SystemInformation.SmallIconSize.Width;
            m_Edit.Top = 4;
            m_Edit.Width = Width - m_Edit.Left - 3 - SystemInformation.VerticalScrollBarWidth;
            m_Edit.Parent = this;
            m_Edit.Visible = false;
            m_Edit.GotFocus += m_Edit_GotFocus;
            m_Edit.LostFocus += m_Edit_LostFocus;
            m_Edit.KeyDown += m_Edit_KeyDown;
            m_Edit.MouseDown += m_Edit_MouseDown;
            m_Edit.BringToFront();
            m_ShellListener.DriveAdded += m_ShellListener_ItemUpdated;
            m_ShellListener.DriveRemoved += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderCreated += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderDeleted += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderRenamed += m_ShellListener_ItemRenamed;
            m_ShellListener.FolderUpdated += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemCreated += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemDeleted += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemRenamed += m_ShellListener_ItemRenamed;
            m_ShellListener.ItemUpdated += m_ShellListener_ItemUpdated;
            m_ShellListener.SharingChanged += m_ShellListener_ItemUpdated;
            m_SelectedFolder = ShellItem.Desktop;
            m_Edit.Text = GetEditString();
            if (m_Computer == null)
            {
                m_Computer = new ShellItem(Environment.SpecialFolder.MyComputer);
            }
            CreateItems();
        }

        [DefaultValue(false)]
        public bool Editable
        {
            get { return m_Editable; }
            set { m_Edit.Visible = m_Editable = value; }
        }

        [DefaultValue(false)]
        public bool ShowFileSystemPath
        {
            get { return m_ShowFileSystemPath; }
            set
            {
                m_ShowFileSystemPath = value;
                m_Combo.Invalidate();
            }
        }

        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem RootFolder
        {
            get { return m_RootFolder; }
            set
            {
                m_RootFolder = value;
                if (!m_RootFolder.IsParentOf(m_SelectedFolder))
                {
                    m_SelectedFolder = m_RootFolder;
                }
                CreateItems();
            }
        }

        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem SelectedFolder
        {
            get { return m_SelectedFolder; }
            set
            {
                if (m_SelectedFolder == value) return;
                m_SelectedFolder = value;
                CreateItems();
                m_Edit.Text = GetEditString();
                NavigateShellView();
                OnChanged();
            }
        }

        [DefaultValue(null), Category("Behaviour")]
        public ShellView ShellView
        {
            get { return m_ShellView; }
            set
            {
                if (m_ShellView != null)
                {
                    m_ShellView.Navigated -= m_ShellView_Navigated;
                }

                m_ShellView = value;

                if (m_ShellView == null) return;
                m_ShellView.Navigated += m_ShellView_Navigated;
                m_ShellView_Navigated(m_ShellView, EventArgs.Empty);
            }
        }

        public event EventHandler Changed;
        public event FilterItemEventHandler FilterItem;

        internal bool ShouldSerializeRootFolder()
        {
            return m_RootFolder != ShellItem.Desktop;
        }

        internal bool ShouldSerializeSelectedFolder()
        {
            return m_SelectedFolder != ShellItem.Desktop;
        }

        private void CreateItems()
        {
            if (m_CreatingItems) return;
            try
            {
                m_CreatingItems = true;
                m_Combo.Items.Clear();
                CreateItem(m_RootFolder, 0);
            }
            finally
            {
                m_CreatingItems = false;
            }
        }

        private void CreateItems(ShellItem folder, int indent)
        {
            var e = folder.GetEnumerator(
                SHCONTF.FOLDERS | SHCONTF.INCLUDEHIDDEN);

            while (e.MoveNext())
            {
                if (ShouldCreateItem(e.Current))
                {
                    CreateItem(e.Current, indent);
                }
            }
        }

        private void CreateItem(ShellItem folder, int indent)
        {
            var index = m_Combo.Items.Add(new ComboItem(folder, indent));

            if (folder == m_SelectedFolder)
            {
                m_Combo.SelectedIndex = index;
            }

            if (ShouldCreateChildren(folder))
            {
                CreateItems(folder, indent + 1);
            }
        }

        private bool ShouldCreateItem(ShellItem folder)
        {
            var e = new FilterItemEventArgs(folder);
            new ShellItem(Environment.SpecialFolder.MyComputer);

            e.Include = false;

            if (ShellItem.Desktop.IsImmediateParentOf(folder) ||
                m_Computer.IsImmediateParentOf(folder))
            {
                e.Include = folder.IsFileSystemAncestor;
            }
            else if (folder == m_SelectedFolder ||
                     folder.IsParentOf(m_SelectedFolder))
            {
                e.Include = true;
            }

            if (FilterItem != null)
            {
                FilterItem(this, e);
            }

            return e.Include;
        }

        private bool ShouldCreateChildren(ShellItem folder)
        {
            return folder == m_Computer ||
                   folder == ShellItem.Desktop ||
                   folder.IsParentOf(m_SelectedFolder);
        }

        private string GetEditString()
        {
            if (m_ShowFileSystemPath && m_SelectedFolder.IsFileSystem)
            {
                return m_SelectedFolder.FileSystemPath;
            }
            return m_SelectedFolder.DisplayName;
        }

        private void NavigateShellView()
        {
            if (m_ShellView == null || m_ChangingLocation) return;
            try
            {
                m_ChangingLocation = true;
                m_ShellView.Navigate(m_SelectedFolder);
            }
            catch (Exception)
            {
                SelectedFolder = m_ShellView.CurrentFolder;
            }
            finally
            {
                m_ChangingLocation = false;
            }
        }

        private void OnChanged()
        {
            if (Changed != null)
            {
                Changed(this, EventArgs.Empty);
            }
        }

        private void m_Combo_Click(object sender, EventArgs e)
        {
            OnClick(e);
        }

        private void m_Combo_DrawItem(object sender, DrawItemEventArgs e)
        {
            var iconWidth = SystemInformation.SmallIconSize.Width;
            var indent = (e.State & DrawItemState.ComboBoxEdit) == 0
                ? iconWidth / 2
                : 0;

            if (e.Index == -1) return;
            string display;
            var item = (ComboItem) m_Combo.Items[e.Index];
            var textColor = SystemColors.WindowText;

            if ((e.State & DrawItemState.ComboBoxEdit) != 0)
            {
                display = m_Editable ? string.Empty : GetEditString();
            }
            else
            {
                display = item.Folder.DisplayName;
            }

            SizeF size = TextRenderer.MeasureText(display, m_Combo.Font);
            var textRect = new Rectangle(
                e.Bounds.Left + iconWidth + item.Indent * indent + 3,
                e.Bounds.Y, (int) size.Width, e.Bounds.Height);
            var textOffset = (int) ((e.Bounds.Height - size.Height) / 2);
            if ((e.State & DrawItemState.ComboBoxEdit) != 0)
            {
                textOffset -= 1;
            }
            if ((e.State & DrawItemState.Selected) != 0)
            {
                e.Graphics.FillRectangle(SystemBrushes.Highlight, textRect);
                textColor = SystemColors.HighlightText;
            }
            else
            {
                e.DrawBackground();
            }

            if ((e.State & DrawItemState.Focus) != 0)
            {
                ControlPaint.DrawFocusRectangle(e.Graphics, textRect);
            }

            SystemImageList.DrawSmallImage(e.Graphics,
                new Point(e.Bounds.Left + item.Indent * indent,
                    e.Bounds.Top),
                item.Folder.GetSystemImageListIndex(ShellIconType.SmallIcon,
                    ShellIconFlags.OverlayIndex),
                (e.State & DrawItemState.Selected) != 0);
            TextRenderer.DrawText(e.Graphics, display, m_Combo.Font,
                new Point(textRect.Left, textRect.Top + textOffset),
                textColor);
        }

        private void m_Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!m_CreatingItems)
            {
                SelectedFolder = ((ComboItem) m_Combo.SelectedItem).Folder;
            }
        }

        private void m_Edit_GotFocus(object sender, EventArgs e)
        {
            m_Edit.SelectAll();
            m_SelectAll = true;
        }

        private void m_Edit_LostFocus(object sender, EventArgs e)
        {
            m_SelectAll = false;
        }

        private void m_Edit_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Enter) return;
            var path = m_Edit.Text;

            if (path == string.Empty ||
                string.Compare(path, "Desktop", StringComparison.OrdinalIgnoreCase) == 0)
            {
                SelectedFolder = ShellItem.Desktop;
                return;
            }

            if (Directory.Exists(path))
            {
                SelectedFolder = new ShellItem(path);
                return;
            }

            path = Path.Combine(m_SelectedFolder.FileSystemPath, path);

            if (Directory.Exists(path))
            {
                SelectedFolder = new ShellItem(path);
            }
        }

        private void m_Edit_MouseDown(object sender, MouseEventArgs e)
        {
            if (m_SelectAll)
            {
                m_Edit.SelectAll();
                m_SelectAll = false;
            }
            else
            {
                m_Edit.SelectionStart = m_Edit.Text.Length;
            }
        }

        private void m_ShellView_Navigated(object sender, EventArgs e)
        {
            if (m_ChangingLocation) return;
            try
            {
                m_ChangingLocation = true;
                SelectedFolder = m_ShellView.CurrentFolder;
                OnChanged();
            }
            finally
            {
                m_ChangingLocation = false;
            }
        }

        private void m_ShellListener_ItemRenamed(object sender, ShellItemChangeEventArgs e)
        {
            CreateItems();
        }

        private void m_ShellListener_ItemUpdated(object sender, ShellItemEventArgs e)
        {
            CreateItems();
        }

        private class ComboItem
        {
            public ComboItem(ShellItem folder, int indent)
            {
                Folder = folder;
                Indent = indent;
            }

            public ShellItem Folder;
            public int Indent;
        }

        private ComboBox m_Combo = new ComboBox();
        private TextBox m_Edit = new TextBox();
        private ShellView m_ShellView;
        private bool m_Editable;
        private ShellItem m_RootFolder = ShellItem.Desktop;
        private ShellItem m_SelectedFolder;
        private bool m_ChangingLocation;
        private bool m_ShowFileSystemPath;
        private bool m_CreatingItems;
        private bool m_SelectAll;
        private ShellNotificationListener m_ShellListener = new ShellNotificationListener();
        private static ShellItem m_Computer;
    }
}