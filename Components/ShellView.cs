using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;
using IonShard.Shell.Core;
using IonShard.Shell.Interop;
using ComTypes = System.Runtime.InteropServices.ComTypes;

namespace IonShard.Shell.Components
{
    public enum ShellViewStyle
    {
        LargeIcon = 1,
        SmallIcon,
        List,
        Details,
        Thumbnail,
        Tile,
        Thumbstrip,
    }

    public class ShellView : Control, INotifyPropertyChanged
    {
        public ShellView()
        {
            History = new ShellHistory();
            m_MultiSelect = true;
            m_View = ShellViewStyle.LargeIcon;
            Size = new Size(250, 200);
            Navigate(ShellItem.Desktop);
        }

        public void CreateNewFolder()
        {
            string name;
            var suffix = 0;

            do
            {
                name = string.Format("{0}\\New Folder ({1})",
                    CurrentFolder.FileSystemPath, ++suffix);
            } while (Directory.Exists(name) || File.Exists(name));

            var result = Shell32.SHCreateDirectory(m_ShellViewWindow, name);

            switch (result)
            {
                case ERROR.FILE_EXISTS:
                case ERROR.ALREADY_EXISTS:
                    throw new IOException("The directory already exists");
                case ERROR.BAD_PATHNAME:
                    throw new IOException("Bad pathname");
                case ERROR.FILENAME_EXCED_RANGE:
                    throw new IOException("The filename is too long");
                case ERROR.SUCCESS:
                    break;
                case ERROR.CANCELLED:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void DeleteSelectedItems()
        {
        }

        public void Navigate(ShellItem folder)
        {
            var e = new NavigatingEventArgs(folder);

            if (Navigating != null)
            {
                Navigating(this, e);
            }

            if (e.Cancel) return;
            var previous = ShellItem;
            ShellItem = folder;

            try
            {
                RecreateShellView();
                History.Add(folder);
                OnNavigated();
            }
            catch (Exception)
            {
                ShellItem = previous;
                RecreateShellView();
                throw;
            }
        }

        public void Navigate(string path)
        {
            Navigate(new ShellItem(path));
        }

        public void Navigate(Environment.SpecialFolder location)
        {
            if (location == Environment.SpecialFolder.MyDocuments)
            {
                location = Environment.SpecialFolder.Personal;
            }

            Navigate(new ShellItem(location));
        }

        public void NavigateBack()
        {
            ShellItem = History.MoveBack();
            RecreateShellView();
            OnNavigated();
        }

        public void NavigateBack(ShellItem folder)
        {
            History.MoveBack(folder);
            ShellItem = folder;
            RecreateShellView();
            OnNavigated();
        }

        public void NavigateForward()
        {
            ShellItem = History.MoveForward();
            RecreateShellView();
            OnNavigated();
        }

        public void NavigateForward(ShellItem folder)
        {
            History.MoveForward(folder);
            ShellItem = folder;
            RecreateShellView();
            OnNavigated();
        }

        public void NavigateParent()
        {
            Navigate(ShellItem.Parent);
        }

        public bool NavigateSelectedFolder()
        {
            var selected = SelectedItems;

            if (selected.Length <= 0) return false;
            foreach (var i in selected.Where(i => i.IsFolder))
            {
                Navigate(i);
                return true;
            }

            return false;
        }

        public void RefreshContents()
        {
            if (ComInterface != null) ComInterface.Refresh();
        }

        public void RenameSelectedItem()
        {
            User32.EnumChildWindows(m_ShellViewWindow, RenameCallback, IntPtr.Zero);
        }

        public void SelectAll()
        {
            foreach (var item in ShellItem)
            {
                ComInterface.SelectItem(item.Pidl, SVSI.SVSI_SELECT);
            }
        }

        [Browsable(false)]
        public bool CanCreateFolder
        {
            get { return ShellItem.IsFileSystem && !ShellItem.IsReadOnly; }
        }

        [Browsable(false)]
        public bool CanNavigateBack
        {
            get { return History.CanNavigateBack; }
        }

        [Browsable(false)]
        public bool CanNavigateForward
        {
            get { return History.CanNavigateForward; }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public bool CanNavigateParent
        {
            get { return ShellItem != ShellItem.Desktop; }
        }

        [Browsable(false)]
        public IShellView ComInterface { get; private set; }

        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem CurrentFolder
        {
            get { return ShellItem; }
            set
            {
                if (value != ShellItem)
                {
                    Navigate(value);
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        public ShellHistory History { get; private set; }

        [Browsable(false)]
        public ShellItem[] SelectedItems
        {
            get
            {
                var CFSTR_SHELLIDLIST =
                    User32.RegisterClipboardFormat("Shell IDList Array");
                var selection = GetSelectionDataObject();

                if (selection == null) return new ShellItem[0];
                var format = new FORMATETC();
                var storage = new STGMEDIUM();

                format.cfFormat = (short) CFSTR_SHELLIDLIST;
                format.dwAspect = DVASPECT.DVASPECT_CONTENT;
                format.lindex = 0;
                format.tymed = TYMED.TYMED_HGLOBAL;

                if (selection.QueryGetData(ref format) != 0) return new ShellItem[0];
                selection.GetData(ref format, out storage);

                var itemCount = Marshal.ReadInt32(storage.unionmember);
                var result = new ShellItem[itemCount];

                for (var n = 0; n < itemCount; ++n)
                {
                    var offset = Marshal.ReadInt32(storage.unionmember,
                        8 + n * 4);
                    result[n] = new ShellItem(
                        ShellItem,
                        (IntPtr) ((int) storage.unionmember + offset));
                }

                GlobalFree(storage.unionmember);
                return result;
            }
        }

        [DefaultValue(true), Category("Behaviour")]
        public bool MultiSelect
        {
            get { return m_MultiSelect; }
            set
            {
                m_MultiSelect = value;
                RecreateShellView();
                OnNavigated();
            }
        }

        [DefaultValue(false), Category("Appearance")]
        public bool ShowWebView
        {
            get { return m_ShowWebView; }
            set
            {
                if (value == m_ShowWebView) return;
                m_ShowWebView = value;
                m_Browser = null;
                RecreateShellView();
                OnNavigated();
            }
        }

        public StatusBar StatusBar
        {
            get { return ((ShellBrowser) GetShellBrowser()).StatusBar; }
            set { ((ShellBrowser) GetShellBrowser()).StatusBar = value; }
        }

        [DefaultValue(ShellViewStyle.LargeIcon), Category("Appearance")]
        public ShellViewStyle View
        {
            get { return m_View; }
            set
            {
                m_View = value;
                RecreateShellView();
                OnNavigated();
            }
        }

        public event FilterItemEventHandler FilterItem;

        public event EventHandler Navigated;

        public event NavigatingEventHandler Navigating;

        public event EventHandler SelectionChanged;

        [Browsable(false)]
        public override Color BackColor
        {
            get { return base.BackColor; }
            set { base.BackColor = value; }
        }

        [Browsable(false)]
        public override Image BackgroundImage
        {
            get { return base.BackgroundImage; }
            set { base.BackgroundImage = value; }
        }

        [Browsable(false)]
        public override ImageLayout BackgroundImageLayout
        {
            get { return base.BackgroundImageLayout; }
            set { base.BackgroundImageLayout = value; }
        }

        event PropertyChangedEventHandler INotifyPropertyChanged.PropertyChanged
        {
            add { m_PropertyChanged += value; }
            remove { m_PropertyChanged -= value; }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (m_ShellViewWindow != IntPtr.Zero)
                {
                    User32.DestroyWindow(m_ShellViewWindow);
                    m_ShellViewWindow = IntPtr.Zero;
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnCreateControl()
        {
            base.OnCreateControl();
            CreateShellView();
            OnNavigated();
        }

        protected override void OnPreviewKeyDown(PreviewKeyDownEventArgs e)
        {
            if (e.KeyData == Keys.Up || e.KeyData == Keys.Down ||
                e.KeyData == Keys.Left || e.KeyData == Keys.Right ||
                e.KeyData == Keys.Enter || e.KeyData == Keys.Delete)
                e.IsInputKey = true;

            if (e.Control && e.KeyCode == Keys.A) SelectAll();

            base.OnPreviewKeyDown(e);
        }

        protected override void OnResize(EventArgs eventargs)
        {
            base.OnResize(eventargs);
            User32.SetWindowPos(m_ShellViewWindow, IntPtr.Zero, 0, 0,
                ClientRectangle.Width, ClientRectangle.Height, 0);
        }

        public override bool PreProcessMessage(ref Message msg)
        {
            const int WM_KEYDOWN = 0x100;
            const int WM_KEYUP = 0x101;
            var keyCode = (Keys) (int) msg.WParam & Keys.KeyCode;

            if (msg.Msg != WM_KEYDOWN && msg.Msg != WM_KEYUP) return base.PreProcessMessage(ref msg);
            switch (keyCode)
            {
                case Keys.F2:
                    RenameSelectedItem();
                    return true;
                case Keys.Delete:
                    DeleteSelectedItems();
                    return true;
                case Keys.KeyCode:
                    break;
                case Keys.Modifiers:
                    break;
                case Keys.None:
                    break;
                case Keys.LButton:
                    break;
                case Keys.RButton:
                    break;
                case Keys.Cancel:
                    break;
                case Keys.MButton:
                    break;
                case Keys.XButton1:
                    break;
                case Keys.XButton2:
                    break;
                case Keys.Back:
                    break;
                case Keys.Tab:
                    break;
                case Keys.LineFeed:
                    break;
                case Keys.Clear:
                    break;
                case Keys.Return:
                    break;
                case Keys.ShiftKey:
                    break;
                case Keys.ControlKey:
                    break;
                case Keys.Menu:
                    break;
                case Keys.Pause:
                    break;
                case Keys.Capital:
                    break;
                case Keys.KanaMode:
                    break;
                case Keys.JunjaMode:
                    break;
                case Keys.FinalMode:
                    break;
                case Keys.HanjaMode:
                    break;
                case Keys.Escape:
                    break;
                case Keys.IMEConvert:
                    break;
                case Keys.IMENonconvert:
                    break;
                case Keys.IMEAccept:
                    break;
                case Keys.IMEModeChange:
                    break;
                case Keys.Space:
                    break;
                case Keys.Prior:
                    break;
                case Keys.Next:
                    break;
                case Keys.End:
                    break;
                case Keys.Home:
                    break;
                case Keys.Left:
                    break;
                case Keys.Up:
                    break;
                case Keys.Right:
                    break;
                case Keys.Down:
                    break;
                case Keys.Select:
                    break;
                case Keys.Print:
                    break;
                case Keys.Execute:
                    break;
                case Keys.Snapshot:
                    break;
                case Keys.Insert:
                    break;
                case Keys.Help:
                    break;
                case Keys.D0:
                    break;
                case Keys.D1:
                    break;
                case Keys.D2:
                    break;
                case Keys.D3:
                    break;
                case Keys.D4:
                    break;
                case Keys.D5:
                    break;
                case Keys.D6:
                    break;
                case Keys.D7:
                    break;
                case Keys.D8:
                    break;
                case Keys.D9:
                    break;
                case Keys.A:
                    break;
                case Keys.B:
                    break;
                case Keys.C:
                    break;
                case Keys.D:
                    break;
                case Keys.E:
                    break;
                case Keys.F:
                    break;
                case Keys.G:
                    break;
                case Keys.H:
                    break;
                case Keys.I:
                    break;
                case Keys.J:
                    break;
                case Keys.K:
                    break;
                case Keys.L:
                    break;
                case Keys.M:
                    break;
                case Keys.N:
                    break;
                case Keys.O:
                    break;
                case Keys.P:
                    break;
                case Keys.Q:
                    break;
                case Keys.R:
                    break;
                case Keys.S:
                    break;
                case Keys.T:
                    break;
                case Keys.U:
                    break;
                case Keys.V:
                    break;
                case Keys.W:
                    break;
                case Keys.X:
                    break;
                case Keys.Y:
                    break;
                case Keys.Z:
                    break;
                case Keys.LWin:
                    break;
                case Keys.RWin:
                    break;
                case Keys.Apps:
                    break;
                case Keys.Sleep:
                    break;
                case Keys.NumPad0:
                    break;
                case Keys.NumPad1:
                    break;
                case Keys.NumPad2:
                    break;
                case Keys.NumPad3:
                    break;
                case Keys.NumPad4:
                    break;
                case Keys.NumPad5:
                    break;
                case Keys.NumPad6:
                    break;
                case Keys.NumPad7:
                    break;
                case Keys.NumPad8:
                    break;
                case Keys.NumPad9:
                    break;
                case Keys.Multiply:
                    break;
                case Keys.Add:
                    break;
                case Keys.Separator:
                    break;
                case Keys.Subtract:
                    break;
                case Keys.Decimal:
                    break;
                case Keys.Divide:
                    break;
                case Keys.F1:
                    break;
                case Keys.F3:
                    break;
                case Keys.F4:
                    break;
                case Keys.F5:
                    break;
                case Keys.F6:
                    break;
                case Keys.F7:
                    break;
                case Keys.F8:
                    break;
                case Keys.F9:
                    break;
                case Keys.F10:
                    break;
                case Keys.F11:
                    break;
                case Keys.F12:
                    break;
                case Keys.F13:
                    break;
                case Keys.F14:
                    break;
                case Keys.F15:
                    break;
                case Keys.F16:
                    break;
                case Keys.F17:
                    break;
                case Keys.F18:
                    break;
                case Keys.F19:
                    break;
                case Keys.F20:
                    break;
                case Keys.F21:
                    break;
                case Keys.F22:
                    break;
                case Keys.F23:
                    break;
                case Keys.F24:
                    break;
                case Keys.NumLock:
                    break;
                case Keys.Scroll:
                    break;
                case Keys.LShiftKey:
                    break;
                case Keys.RShiftKey:
                    break;
                case Keys.LControlKey:
                    break;
                case Keys.RControlKey:
                    break;
                case Keys.LMenu:
                    break;
                case Keys.RMenu:
                    break;
                case Keys.BrowserBack:
                    break;
                case Keys.BrowserForward:
                    break;
                case Keys.BrowserRefresh:
                    break;
                case Keys.BrowserStop:
                    break;
                case Keys.BrowserSearch:
                    break;
                case Keys.BrowserFavorites:
                    break;
                case Keys.BrowserHome:
                    break;
                case Keys.VolumeMute:
                    break;
                case Keys.VolumeDown:
                    break;
                case Keys.VolumeUp:
                    break;
                case Keys.MediaNextTrack:
                    break;
                case Keys.MediaPreviousTrack:
                    break;
                case Keys.MediaStop:
                    break;
                case Keys.MediaPlayPause:
                    break;
                case Keys.LaunchMail:
                    break;
                case Keys.SelectMedia:
                    break;
                case Keys.LaunchApplication1:
                    break;
                case Keys.LaunchApplication2:
                    break;
                case Keys.OemSemicolon:
                    break;
                case Keys.Oemplus:
                    break;
                case Keys.Oemcomma:
                    break;
                case Keys.OemMinus:
                    break;
                case Keys.OemPeriod:
                    break;
                case Keys.OemQuestion:
                    break;
                case Keys.Oemtilde:
                    break;
                case Keys.OemOpenBrackets:
                    break;
                case Keys.OemPipe:
                    break;
                case Keys.OemCloseBrackets:
                    break;
                case Keys.OemQuotes:
                    break;
                case Keys.Oem8:
                    break;
                case Keys.OemBackslash:
                    break;
                case Keys.ProcessKey:
                    break;
                case Keys.Packet:
                    break;
                case Keys.Attn:
                    break;
                case Keys.Crsel:
                    break;
                case Keys.Exsel:
                    break;
                case Keys.EraseEof:
                    break;
                case Keys.Play:
                    break;
                case Keys.Zoom:
                    break;
                case Keys.NoName:
                    break;
                case Keys.Pa1:
                    break;
                case Keys.OemClear:
                    break;
                case Keys.Shift:
                    break;
                case Keys.Control:
                    break;
                case Keys.Alt:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return base.PreProcessMessage(ref msg);
        }

        protected override void WndProc(ref Message m)
        {
            const int CWM_GETISHELLBROWSER = 0x407;
            if (m.Msg == CWM_GETISHELLBROWSER)
            {
                m.Result = Marshal.GetComInterfaceForObject(m_Browser,
                    typeof(IShellBrowser));
            }
            else
            {
                base.WndProc(ref m);
            }
        }

        internal bool IncludeItem(IntPtr pidl)
        {
            if (FilterItem == null) return true;
            var e = new FilterItemEventArgs(
                new ShellItem(ShellItem, pidl));
            FilterItem(this, e);
            return e.Include;
        }

        internal new void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
        }

        internal void OnSelectionChanged()
        {
            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        internal ShellItem ShellItem { get; private set; }

        private void CreateShellView()
        {
            var previous = ComInterface;
            var bounds = ClientRectangle;
            var folderSettings = new FOLDERSETTINGS();
            ComInterface = CreateViewObject(ShellItem, Handle);
            folderSettings.ViewMode = (FOLDERVIEWMODE) m_View;

            if (!m_ShowWebView)
            {
                folderSettings.fFlags |= FOLDERFLAGS.NOWEBVIEW;
            }

            if (!m_MultiSelect)
            {
                folderSettings.fFlags |= FOLDERFLAGS.SINGLESEL;
            }

            try
            {
                ComInterface.CreateViewWindow(previous, ref folderSettings,
                    GetShellBrowser(), ref bounds,
                    out m_ShellViewWindow);
            }
            catch (COMException ex)
            {
                if (ex.ErrorCode == unchecked((int) 0x800704C7U))
                {
                    throw new UserAbortException(ex);
                }
            }

            ComInterface.UIActivate(1);
            if (DesignMode)
            {
                User32.EnableWindow(m_ShellViewWindow, false);
            }

            if (previous != null) previous.DestroyViewWindow();
        }

        private void RecreateShellView()
        {
            if (ComInterface != null)
            {
                CreateShellView();
                OnNavigated();
            }

            if (m_PropertyChanged != null)
            {
                m_PropertyChanged(this,
                    new PropertyChangedEventArgs("CurrentFolder"));
            }
        }

        private static bool RenameCallback(IntPtr hwnd, IntPtr lParam)
        {
            var itemCount = User32.SendMessage(hwnd,
                MSG.LVM_GETITEMCOUNT, 0, 0);

            for (var n = 0; n < itemCount; ++n)
            {
                var item = new LVITEMA {mask = LVIF.LVIF_STATE, iItem = n, stateMask = LVIS.LVIS_SELECTED};
                User32.SendMessage(hwnd, MSG.LVM_GETITEMA,
                    0, ref item);

                if (item.state == 0) continue;
                User32.SendMessage(hwnd, MSG.LVM_EDITLABEL, n, 0);
                return false;
            }

            return true;
        }

        private ComTypes.IDataObject GetSelectionDataObject()
        {
            IntPtr result;

            if (ComInterface == null)
            {
                return null;
            }

            ComInterface.GetItemObject(SVGIO.SVGIO_SELECTION,
                typeof(ComTypes.IDataObject).GUID, out result);

            if (result == IntPtr.Zero) return null;
            var wrapped =
                (ComTypes.IDataObject)
                Marshal.GetTypedObjectForIUnknown(result,
                    typeof(ComTypes.IDataObject));
            return wrapped;
        }

        private IShellBrowser GetShellBrowser()
        {
            return m_Browser ?? (m_Browser = m_ShowWebView ? new ShellBrowser(this) : new DialogShellBrowser(this));
        }

        private void OnNavigated()
        {
            if (Navigated != null)
            {
                Navigated(this, EventArgs.Empty);
            }
        }

        private bool ShouldSerializeCurrentFolder()
        {
            return ShellItem != ShellItem.Desktop;
        }

        private static IShellView CreateViewObject(ShellItem folder, IntPtr hwndOwner)
        {
            var result = folder.GetIShellFolder().CreateViewObject(hwndOwner,
                typeof(IShellView).GUID);
            return (IShellView)
                Marshal.GetTypedObjectForIUnknown(result,
                    typeof(IShellView));
        }

        [DllImport("kernel32.dll")]
        private static extern IntPtr GlobalFree(IntPtr hMem);

        private bool m_MultiSelect;
        private ShellBrowser m_Browser;
        private IntPtr m_ShellViewWindow;
        private bool m_ShowWebView;
        private ShellViewStyle m_View;
        private PropertyChangedEventHandler m_PropertyChanged;
    }

    public class FilterItemEventArgs : EventArgs
    {
        internal FilterItemEventArgs(ShellItem item)
        {
            Item = item;
        }

        public bool Include
        {
            get { return m_Include; }
            set { m_Include = value; }
        }

        public ShellItem Item { get; private set; }

        private bool m_Include = true;
    }

    public class NavigatingEventArgs : EventArgs
    {
        public NavigatingEventArgs(ShellItem folder)
        {
            Folder = folder;
        }

        public bool Cancel { get; set; }

        public ShellItem Folder { get; set; }
    }

    public class UserAbortException : ExternalException
    {
        public UserAbortException(Exception e)
            : base("User aborted", e)
        {
        }
    }

    public delegate void FilterItemEventHandler(object sender,
        FilterItemEventArgs e);

    public delegate void NavigatingEventHandler(object sender,
        NavigatingEventArgs e);
}