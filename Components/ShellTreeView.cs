using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IonShard.Shell.Core;
using IonShard.Shell.Interop;
using Microsoft.Win32;
using ComTypes = System.Runtime.InteropServices.ComTypes;
using IDropTarget = IonShard.Shell.Interop.IDropTarget;

namespace IonShard.Shell.Components
{
    public class ShellTreeView : Control, IDropSource, IDropTarget
    {
        public ShellTreeView()
        {
            m_TreeView = new TreeView
            {
                Dock = DockStyle.Fill,
                HideSelection = false,
                HotTracking = true,
                Parent = this,
                ShowRootLines = false
            };
            m_TreeView.AfterSelect += m_TreeView_AfterSelect;
            m_TreeView.BeforeExpand += m_TreeView_BeforeExpand;
            m_TreeView.ItemDrag += m_TreeView_ItemDrag;
            m_TreeView.MouseDown += m_TreeView_MouseDown;
            m_TreeView.MouseUp += m_TreeView_MouseUp;
            m_ScrollTimer.Interval = 250;
            m_ScrollTimer.Tick += m_ScrollTimer_Tick;
            Size = new Size(120, 100);
            SystemImageList.UseSystemImageList(m_TreeView);
            m_ShellListener.DriveAdded += m_ShellListener_ItemUpdated;
            m_ShellListener.DriveRemoved += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderCreated += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderDeleted += m_ShellListener_ItemUpdated;
            m_ShellListener.FolderRenamed += m_ShellListener_ItemRenamed;
            m_ShellListener.FolderUpdated += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemCreated += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemDeleted += m_ShellListener_ItemUpdated;
            m_ShellListener.ItemRenamed += m_ShellListener_ItemRenamed;
            m_ShellListener.ItemUpdated += m_ShellListener_ItemUpdated;
            m_ShellListener.SharingChanged += m_ShellListener_ItemUpdated;
            m_TreeView.AllowDrop = true;
            m_TreeView.AllowDrop = false;
            CreateItems();
        }

        public void RefreshContents()
        {
            RefreshItem(m_TreeView.Nodes[0]);
        }

        [DefaultValue(false)]
        public override bool AllowDrop
        {
            get { return m_AllowDrop; }
            set
            {
                if (value == m_AllowDrop) return;
                m_AllowDrop = value;

                Marshal.ThrowExceptionForHR(
                    m_AllowDrop
                        ? Ole32.RegisterDragDrop(m_TreeView.Handle, this)
                        : Ole32.RevokeDragDrop(m_TreeView.Handle));
            }
        }

        [DefaultValue(true)]
        [Category("Appearance")]
        public bool HotTracking
        {
            get { return m_TreeView.HotTracking; }
            set { m_TreeView.HotTracking = value; }
        }

        [Category("Appearance")]
        public ShellItem RootFolder
        {
            get { return m_RootFolder; }
            set
            {
                m_RootFolder = value;
                CreateItems();
            }
        }

        [DefaultValue(null), Category("Behaviour")]
        public ShellView ShellView
        {
            get { return m_ShellView; }
            set
            {
                if (m_ShellView != null)
                {
                    m_ShellView.Navigated -= m_ShellView_Navigated;
                }

                m_ShellView = value;

                if (m_ShellView == null) return;
                m_ShellView.Navigated += m_ShellView_Navigated;
                m_ShellView_Navigated(m_ShellView, EventArgs.Empty);
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem SelectedFolder
        {
            get { return (ShellItem) m_TreeView.SelectedNode.Tag; }
            set { SelectItem(value); }
        }

        [DefaultValue(ShowHidden.System), Category("Appearance")]
        public ShowHidden ShowHidden
        {
            get { return m_ShowHidden; }
            set
            {
                m_ShowHidden = value;
                RefreshContents();
            }
        }

        public event EventHandler SelectionChanged;

        HResult IDropSource.QueryContinueDrag(bool fEscapePressed, int grfKeyState)
        {
            if (fEscapePressed)
            {
                return HResult.DRAGDROP_S_CANCEL;
            }
            return (grfKeyState & (int) (MK.MK_LBUTTON | MK.MK_RBUTTON)) == 0 ? HResult.DRAGDROP_S_DROP : HResult.S_OK;
        }

        HResult IDropSource.GiveFeedback(int dwEffect)
        {
            return HResult.DRAGDROP_S_USEDEFAULTCURSORS;
        }

        void IDropTarget.DragEnter(ComTypes.IDataObject pDataObj,
            int grfKeyState, Point pt,
            ref int pdwEffect)
        {
            var clientLocation = m_TreeView.PointToClient(pt);
            var node = m_TreeView.HitTest(clientLocation).Node;

            DragTarget.Data = pDataObj;
            m_TreeView.HideSelection = true;

            if (node != null)
            {
                m_DragTarget = new DragTarget(node, grfKeyState, pt,
                    ref pdwEffect);
            }
            else
            {
                pdwEffect = 0;
            }
        }

        void IDropTarget.DragOver(int grfKeyState, Point pt,
            ref int pdwEffect)
        {
            var clientLocation = m_TreeView.PointToClient(pt);
            var node = m_TreeView.HitTest(clientLocation).Node;

            CheckDragScroll(clientLocation);

            if (node != null)
            {
                if (m_DragTarget == null ||
                    node != m_DragTarget.Node)
                {
                    if (m_DragTarget != null)
                    {
                        m_DragTarget.Dispose();
                    }

                    m_DragTarget = new DragTarget(node, grfKeyState,
                        pt, ref pdwEffect);
                }
                else
                {
                    m_DragTarget.DragOver(grfKeyState, pt, ref pdwEffect);
                }
            }
            else
            {
                pdwEffect = 0;
            }
        }

        void IDropTarget.DragLeave()
        {
            if (m_DragTarget != null)
            {
                m_DragTarget.Dispose();
                m_DragTarget = null;
            }
            m_TreeView.HideSelection = false;
        }

        void IDropTarget.Drop(ComTypes.IDataObject pDataObj,
            int grfKeyState, Point pt,
            ref int pdwEffect)
        {
            if (m_DragTarget == null) return;
            m_DragTarget.Drop(pDataObj, grfKeyState, pt,
                ref pdwEffect);
            m_DragTarget.Dispose();
            m_DragTarget = null;
        }

        [Browsable(false)]
        [EditorBrowsable(EditorBrowsableState.Never)]
        public override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void CreateItems()
        {
            m_TreeView.BeginUpdate();

            try
            {
                m_TreeView.Nodes.Clear();
                CreateItem(null, m_RootFolder);
                m_TreeView.Nodes[0].Expand();
                m_TreeView.SelectedNode = m_TreeView.Nodes[0];
            }
            finally
            {
                m_TreeView.EndUpdate();
            }
        }

        private void CreateItem(TreeNode parent, ShellItem folder)
        {
            var displayName = folder.DisplayName;

            var node = parent != null ? InsertNode(parent, folder, displayName) : m_TreeView.Nodes.Add(displayName);

            if (folder.HasSubFolders)
            {
                node.Nodes.Add("");
            }

            node.Tag = folder;
            SetNodeImage(node);
        }

        private void CreateChildren(TreeNode node)
        {
            if (node.Nodes.Count != 1 || node.Nodes[0].Tag != null) return;
            var folder = (ShellItem) node.Tag;
            var e = GetFolderEnumerator(folder);

            node.Nodes.Clear();
            while (e.MoveNext())
            {
                CreateItem(node, e.Current);
            }
        }

        private void RefreshItem(TreeNode node)
        {
            try
            {
                var folder = (ShellItem) node.Tag;
                node.Text = folder.DisplayName;
                SetNodeImage(node);

                if (NodeHasChildren(node))
                {
                    var e = GetFolderEnumerator(folder);
                    var nodesToRemove = new ArrayList(node.Nodes);

                    while (e.MoveNext())
                    {
                        var childNode = FindItem(e.Current, node);

                        if (childNode != null)
                        {
                            RefreshItem(childNode);
                            nodesToRemove.Remove(childNode);
                        }
                        else
                        {
                            CreateItem(node, e.Current);
                        }
                    }

                    foreach (TreeNode n in nodesToRemove)
                    {
                        n.Remove();
                    }
                }
                else if (node.Nodes.Count == 0)
                {
                    if (folder.HasSubFolders)
                    {
                        node.Nodes.Add("");
                    }
                }
            }
            catch
            {
                // ignored
            }
        }

        private static TreeNode InsertNode(TreeNode parent, ShellItem folder, string displayName)
        {
            var parentFolder = (ShellItem) parent.Tag;
            var folderRelPidl = Shell32.ILFindLastID(folder.Pidl);
            var result = (from TreeNode child in parent.Nodes
                             let childFolder = (ShellItem) child.Tag
                             let childRelPidl = Shell32.ILFindLastID(childFolder.Pidl)
                             let compare = parentFolder.GetIShellFolder().CompareIDs(0, folderRelPidl, childRelPidl)
                             where compare < 0
                             select parent.Nodes.Insert(child.Index, displayName)).FirstOrDefault() ??
                         parent.Nodes.Add(displayName);

            return result;
        }

        private bool ShouldShowHidden()
        {
            if (m_ShowHidden != ShowHidden.System) return m_ShowHidden == ShowHidden.True;
            var reg = Registry.CurrentUser.OpenSubKey(
                @"Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced");

            if (reg != null)
            {
                return (int) reg.GetValue("Hidden", 2) == 1;
            }
            return false;
        }

        private IEnumerator<ShellItem> GetFolderEnumerator(ShellItem folder)
        {
            var filter = SHCONTF.FOLDERS;
            if (ShouldShowHidden()) filter |= SHCONTF.INCLUDEHIDDEN;
            return folder.GetEnumerator(filter);
        }

        private void SetNodeImage(TreeNode node)
        {
            var itemInfo = new TVITEMW();
            var folder = (ShellItem) node.Tag;
            itemInfo.mask = TVIF.TVIF_IMAGE | TVIF.TVIF_SELECTEDIMAGE |
                            TVIF.TVIF_STATE;
            itemInfo.hItem = node.Handle;
            itemInfo.iImage = folder.GetSystemImageListIndex(
                ShellIconType.SmallIcon, ShellIconFlags.OverlayIndex);
            itemInfo.iSelectedImage = folder.GetSystemImageListIndex(
                ShellIconType.SmallIcon, ShellIconFlags.OpenIcon);
            itemInfo.state = (TVIS) (itemInfo.iImage >> 16);
            itemInfo.stateMask = TVIS.TVIS_OVERLAYMASK;
            User32.SendMessage(m_TreeView.Handle, MSG.TVM_SETITEMW,
                0, ref itemInfo);
        }

        private void SelectItem(ShellItem value)
        {
            var node = m_TreeView.Nodes[0];
            var folder = (ShellItem) node.Tag;

            if (folder == value)
            {
                m_TreeView.SelectedNode = node;
            }
            else
            {
                SelectItem(node, value);
            }
        }

        private void SelectItem(TreeNode node, ShellItem value)
        {
            CreateChildren(node);

            foreach (TreeNode child in node.Nodes)
            {
                var folder = (ShellItem) child.Tag;

                if (folder == value)
                {
                    m_TreeView.SelectedNode = child;
                    child.EnsureVisible();
                    child.Expand();
                    return;
                }
                if (!folder.IsParentOf(value)) continue;
                SelectItem(child, value);
                return;
            }
        }

        private static TreeNode FindItem(ShellItem item, TreeNode parent)
        {
            if ((ShellItem) parent.Tag == item)
            {
                return parent;
            }

            foreach (TreeNode node in parent.Nodes)
            {
                if ((ShellItem) node.Tag == item)
                {
                    return node;
                }
                var found = FindItem(item, node);
                if (found != null) return found;
            }
            return null;
        }

        private static bool NodeHasChildren(TreeNode node)
        {
            return node.Nodes.Count > 0 && node.Nodes[0].Tag != null;
        }

        private void ScrollTreeView(ScrollDirection direction)
        {
            User32.SendMessage(m_TreeView.Handle, MSG.WM_VSCROLL,
                (int) direction, 0);
        }

        private void CheckDragScroll(Point location)
        {
            var scrollArea = (int) (m_TreeView.Nodes[0].Bounds.Height * 1.5);
            var scroll = ScrollDirection.None;

            if (location.Y < scrollArea)
            {
                scroll = ScrollDirection.Up;
            }
            else if (location.Y > m_TreeView.ClientRectangle.Height - scrollArea)
            {
                scroll = ScrollDirection.Down;
            }

            if (scroll != ScrollDirection.None)
            {
                if (m_ScrollDirection == ScrollDirection.None)
                {
                    ScrollTreeView(scroll);
                    m_ScrollTimer.Enabled = true;
                }
            }
            else
            {
                m_ScrollTimer.Enabled = false;
            }

            m_ScrollDirection = scroll;
        }

        private bool ShouldSerializeRootFolder()
        {
            return m_RootFolder != ShellItem.Desktop;
        }

        private void m_TreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (m_ShellView != null && !m_Navigating)
            {
                m_Navigating = true;
                try
                {
                    m_ShellView.CurrentFolder = SelectedFolder;
                }
                catch (Exception)
                {
                    SelectedFolder = m_ShellView.CurrentFolder;
                }
                finally
                {
                    m_Navigating = false;
                }
            }

            if (SelectionChanged != null)
            {
                SelectionChanged(this, EventArgs.Empty);
            }
        }

        private void m_TreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            try
            {
                CreateChildren(e.Node);
            }
            catch (Exception)
            {
                e.Cancel = true;
            }
        }

        private void m_TreeView_ItemDrag(object sender, ItemDragEventArgs e)
        {
            var node = (TreeNode) e.Item;
            var folder = (ShellItem) node.Tag;
            DragDropEffects effect;

            Ole32.DoDragDrop(folder.GetIDataObject(), this,
                DragDropEffects.All, out effect);
        }

        private void m_TreeView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                m_RightClickNode = m_TreeView.GetNodeAt(e.Location);
            }
        }

        private void m_TreeView_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;
            var node = m_TreeView.GetNodeAt(e.Location);

            if (node == null || node != m_RightClickNode) return;
            var folder = (ShellItem) node.Tag;
            new ShellContextMenu(folder).ShowContextMenu(m_TreeView, e.Location);
        }

        private void m_ScrollTimer_Tick(object sender, EventArgs e)
        {
            ScrollTreeView(m_ScrollDirection);
        }

        private void m_ShellListener_ItemRenamed(object sender, ShellItemChangeEventArgs e)
        {
            var node = FindItem(e.OldItem, m_TreeView.Nodes[0]);
            if (node != null) RefreshItem(node);
        }

        private void m_ShellListener_ItemUpdated(object sender, ShellItemEventArgs e)
        {
            var parent = FindItem(e.Item.Parent, m_TreeView.Nodes[0]);
            if (parent != null) RefreshItem(parent);
        }

        private void m_ShellView_Navigated(object sender, EventArgs e)
        {
            if (m_Navigating) return;
            m_Navigating = true;
            SelectedFolder = m_ShellView.CurrentFolder;
            m_Navigating = false;
        }

        private enum ScrollDirection
        {
            None = -1,
            Up,
            Down
        }

        private class DragTarget : IDisposable
        {
            public DragTarget(TreeNode node,
                int keyState, Point pt,
                ref int effect)
            {
                Node = node;
                Node.BackColor = SystemColors.Highlight;
                Node.ForeColor = SystemColors.HighlightText;

                m_DragExpandTimer = new Timer {Interval = 1000};
                m_DragExpandTimer.Tick += m_DragExpandTimer_Tick;
                m_DragExpandTimer.Start();

                try
                {
                    m_DropTarget = Folder.GetIDropTarget(node.TreeView);
                    m_DropTarget.DragEnter(m_Data, keyState, pt, ref effect);
                }
                catch
                {
                    // ignored
                }
            }

            public void Dispose()
            {
                Node.BackColor = Node.TreeView.BackColor;
                Node.ForeColor = Node.TreeView.ForeColor;
                m_DragExpandTimer.Dispose();

                if (m_DropTarget != null)
                {
                    m_DropTarget.DragLeave();
                }
            }

            public void DragOver(int keyState, Point pt, ref int effect)
            {
                if (m_DropTarget != null)
                {
                    m_DropTarget.DragOver(keyState, pt, ref effect);
                }
                else
                {
                    effect = 0;
                }
            }

            public void Drop(ComTypes.IDataObject data, int keyState,
                Point pt, ref int effect)
            {
                m_DropTarget.Drop(data, keyState, pt, ref effect);
            }

            private ShellItem Folder
            {
                get { return (ShellItem) Node.Tag; }
            }

            public TreeNode Node { get; private set; }

            public static ComTypes.IDataObject Data
            {
                set { m_Data = value; }
            }

            private void m_DragExpandTimer_Tick(object sender, EventArgs e)
            {
                Node.Expand();
                m_DragExpandTimer.Stop();
            }

            private IDropTarget m_DropTarget;
            private Timer m_DragExpandTimer;
            private static ComTypes.IDataObject m_Data;
        }

        private TreeView m_TreeView;
        private TreeNode m_RightClickNode;
        private DragTarget m_DragTarget;
        private Timer m_ScrollTimer = new Timer();
        private ScrollDirection m_ScrollDirection = ScrollDirection.None;
        private ShellItem m_RootFolder = ShellItem.Desktop;
        private ShellView m_ShellView;
        private ShowHidden m_ShowHidden = ShowHidden.System;
        private bool m_Navigating;
        private bool m_AllowDrop;
        private ShellNotificationListener m_ShellListener = new ShellNotificationListener();
    }

    public enum ShowHidden
    {
        False,
        True,
        System
    }
}