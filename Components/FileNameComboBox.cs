using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using IonShard.Shell.Core;

namespace IonShard.Shell.Components
{
    /// <summary>
    /// A filename combo box suitable for use in file Open/Save dialogs.
    /// </summary>
    /// 
    /// <remarks>
    /// <para>
    /// This control extends the <see cref="ComboBox"/> class to provide
    /// auto-completion of filenames based on the folder selected in a
    /// <see cref="ShellView"/>. The control also automatically navigates 
    /// the ShellView control when the user types a folder path.
    /// </para>
    /// </remarks>
    public class FileNameComboBox : ComboBox
    {
        [Category("Behaviour")]
        [DefaultValue(null)]
        public FileFilterComboBox FilterControl { get; set; }

        [Category("Behaviour")]
        [DefaultValue(null)]
        public ShellView ShellView
        {
            get { return _mShellView; }
            set
            {
                DisconnectEventHandlers();
                _mShellView = value;
                ConnectEventHandlers();
            }
        }

        public event EventHandler FileNameEntered;

        protected override bool IsInputKey(Keys keyData)
        {
            return keyData == Keys.Enter || base.IsInputKey(keyData);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.KeyCode == Keys.Enter)
            {
                if (Text.Length > 0 && !Open(Text) &&
                    FilterControl != null)
                {
                    FilterControl.Filter = Text;
                }
            }

            _mTryAutoComplete = false;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            _mTryAutoComplete = char.IsLetterOrDigit(e.KeyChar);
        }

        protected override void OnTextChanged(EventArgs e)
        {
            base.OnTextChanged(e);
            if (!_mTryAutoComplete) return;
            try
            {
                AutoComplete();
            }
            catch
            {
                // ignored
            }
        }

        private void AutoComplete()
        {
            var rooted = true;
            if (Text == string.Empty ||
                Text.IndexOfAny(new[] {'?', '*'}) != -1)
            {
                return;
            }
            var path = Path.GetDirectoryName(Text);
            var pattern = Path.GetFileName(Text);
            if (string.IsNullOrEmpty(path) && _mShellView != null && _mShellView.CurrentFolder.IsFileSystem &&
                _mShellView.CurrentFolder != ShellItem.Desktop)
            {
                path = _mShellView.CurrentFolder.FileSystemPath;
                pattern = Text;
                rooted = false;
            }
            var matches = Directory.GetFiles(path, pattern + '*');

            for (var n = 0; n < 2; ++n)
            {
                if (matches.Length > 0)
                {
                    var currentLength = Text.Length;
                    Text = rooted ? matches[0] : Path.GetFileName(matches[0]);
                    SelectionStart = currentLength;
                    if (Text != null) SelectionLength = Text.Length;
                    break;
                }
                matches = Directory.GetDirectories(path, pattern + '*');
            }
        }

        private void ConnectEventHandlers()
        {
            if (_mShellView != null)
            {
                _mShellView.SelectionChanged += m_ShellView_SelectionChanged;
            }
        }

        private void DisconnectEventHandlers()
        {
            if (_mShellView != null)
            {
                _mShellView.SelectionChanged -= m_ShellView_SelectionChanged;
            }
        }

        private bool Open(string path)
        {
            var result = false;

            if (File.Exists(path))
            {
                if (FileNameEntered != null)
                {
                    FileNameEntered(this, EventArgs.Empty);
                }
                result = true;
            }
            else if (Directory.Exists(path))
            {
                if (_mShellView != null)
                {
                    _mShellView.Navigate(path);
                    Text = string.Empty;
                    result = true;
                }
            }
            else
            {
                OpenParentOf(path);
                Text = Path.GetFileName(path);
            }

            if (_mShellView != null && !Path.IsPathRooted(path) && _mShellView.CurrentFolder.IsFileSystem)
            {
                result = Open(Path.Combine(_mShellView.CurrentFolder.FileSystemPath,
                    path));
            }

            return result;
        }

        private void OpenParentOf(string path)
        {
            var parent = Path.GetDirectoryName(path);

            if (string.IsNullOrEmpty(parent) || !Directory.Exists(parent)) return;
            if (_mShellView != null)
            {
                _mShellView.Navigate(parent);
            }
        }

        private void m_ShellView_SelectionChanged(object sender, EventArgs e)
        {
            if (_mShellView.SelectedItems.Length > 0 &&
                !_mShellView.SelectedItems[0].IsFolder &&
                _mShellView.SelectedItems[0].IsFileSystem)
            {
                Text = Path.GetFileName(_mShellView.SelectedItems[0].FileSystemPath);
            }
        }

        private ShellView _mShellView;
        private bool _mTryAutoComplete;
    }
}