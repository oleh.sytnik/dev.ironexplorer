using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using IonShard.Shell.Core;

namespace IonShard.Shell.Components
{
    public partial class FileDialogToolbar : UserControl
    {
        public FileDialogToolbar()
        {
            InitializeComponent();
            Dock = DockStyle.Top;
            toolStrip.Renderer = new CustomRenderer();
        }

        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem RootFolder
        {
            get { return shellComboBox.RootFolder; }
            set { shellComboBox.RootFolder = value; }
        }

        [Editor(typeof(ShellItemEditor), typeof(UITypeEditor))]
        public ShellItem SelectedFolder
        {
            get { return shellComboBox.SelectedFolder; }
            set { shellComboBox.SelectedFolder = value; }
        }

        [DefaultValue(null), Category("Behaviour")]
        public ShellView ShellView
        {
            get { return shellComboBox.ShellView; }
            set
            {
                shellComboBox.ShellView = value;
                shellComboBox.Enabled = viewMenuButton.Enabled = value != null;
                UpdateButtons();
            }
        }

        public event FilterItemEventHandler FilterItem
        {
            add { shellComboBox.FilterItem += value; }
            remove { shellComboBox.FilterItem -= value; }
        }

        private bool ShouldSerializeRootFolder()
        {
            return shellComboBox.ShouldSerializeRootFolder();
        }

        private bool ShouldSerializeSelectedFolder()
        {
            return shellComboBox.ShouldSerializeSelectedFolder();
        }

        private void UpdateButtons()
        {
            if (ShellView == null) return;
            backButton.Enabled = ShellView.CanNavigateBack;
            upButton.Enabled = ShellView.CanNavigateParent;
            newFolderButton.Enabled = ShellView.CanCreateFolder;
        }

        private void shellComboBox_Changed(object sender, EventArgs e)
        {
            UpdateButtons();
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            ShellView.NavigateBack();
        }

        private void upButton_Click(object sender, EventArgs e)
        {
            ShellView.NavigateParent();
        }

        private void newFolderButton_Click(object sender, EventArgs e)
        {
            ShellView.CreateNewFolder();
        }

        private void viewThumbnailsMenu_Click(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem) sender;
            ShellView.View = (ShellViewStyle) Convert.ToInt32(item.Tag);
        }

        private class CustomRenderer : ToolStripSystemRenderer
        {
            protected override void OnRenderToolStripBorder(ToolStripRenderEventArgs e)
            {
            }
        }
    }
}