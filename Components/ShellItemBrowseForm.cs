using System;
using System.Windows.Forms;

namespace IonShard.Shell.Core
{
    internal partial class ShellItemBrowseForm : Form
    {
        public ShellItemBrowseForm()
        {
            InitializeComponent();

            var manager = new KnownFolderManager();

            SystemImageList.UseSystemImageList(knownFolderList);
            foreach (var knownFolder in manager)
            {
                try
                {
                    var shellItem = knownFolder.CreateShellItem();
                    var item = knownFolderList.Items.Add(knownFolder.Name,
                        shellItem.GetSystemImageListIndex(ShellIconType.LargeIcon, 0));

                    item.Tag = knownFolder;

                    switch (item.Text)
                    {
                        case @"Personal":
                            item.Text = @"Personal (My Documents)";
                            item.Group = knownFolderList.Groups["common"];
                            break;
                        case "MyComputerFolder":
                        case "Downloads":
                        case "Desktop":
                            item.Group = knownFolderList.Groups["common"];
                            break;
                        default:
                            item.Group = knownFolderList.Groups["all"];
                            break;
                    }
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        public ShellItem SelectedItem { get; private set; }

        private void SaveSelection()
        {
            if (tabControl.SelectedTab == knownFoldersPage)
            {
                SelectedItem = ((KnownFolder) knownFolderList.SelectedItems[0].Tag)
                    .CreateShellItem();
            }
            else
            {
                SelectedItem = allFilesView.SelectedItems[0];
            }
        }

        private void knownFolderList_DoubleClick(object sender, EventArgs e)
        {
            if (knownFolderList.SelectedItems.Count <= 0) return;
            SaveSelection();
            DialogResult = DialogResult.OK;
        }

        private void knownFolderList_SelectedIndexChanged(object sender, EventArgs e)
        {
            okButton.Enabled = knownFolderList.SelectedItems.Count > 0;
        }

        private void fileBrowseView_SelectionChanged(object sender, EventArgs e)
        {
            okButton.Enabled = allFilesView.SelectedItems.Length > 0;
        }

        private void tabControl_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == knownFoldersPage)
            {
                okButton.Enabled = knownFolderList.SelectedItems.Count > 0;
            }
            else if (e.TabPage == allFilesPage)
            {
                okButton.Enabled = allFilesView.SelectedItems.Length > 0;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            SaveSelection();
            DialogResult = DialogResult.OK;
        }
    }
}